/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Behaviour.APIConnection;
import Behaviour.Address;
import Behaviour.Company;
import Behaviour.Event;
import Behaviour.IListable;
import Behaviour.Login;
import Behaviour.MotionPanel;
import Behaviour.Ticket;
import Behaviour.User;
import Behaviour.Venue;
import Lists.AddressList;
import Lists.CompanyList;
import Lists.EventList;
import Lists.IList;
import Lists.LoginList;
import Lists.TicketList;
import Lists.UserList;
import Lists.VenueList;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author mvizard
 */
public class ListGUI extends javax.swing.JFrame{
    
    //IList list;
    EventList eventList;
    CompanyList companyList;
    VenueList venueList;
    TicketList ticketList;
    UserList userList;
    LoginList loginList;
    AddressList addressList;
    
    IListable listType;
    
    
    DefaultListModel<String> listModel;
    DefaultComboBoxModel<String> cmbModel;
    
    JFrame parent;
    APIConnection api;
    
    //Colours (with a u) for the UI
    Color primary = new Color(0x233e68);
    Color primaryLighter = new Color(0x2d5086);
    
    public ListGUI() {
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

        //set panel to enable movement of window
        JPanel panel = new MotionPanel(this);
        panel.setBounds(0, 0, this.getWidth(), 23);
        panel.setBackground(primary);
        this.add(panel);
        
        
        //txtLiveHelp.setVisible(false);        
        //this.setListType(new Ticket());
        
        Container thisPane = this.getContentPane();
        thisPane.setBackground(primaryLighter);
        
        btnEventDone.setVisible(false);
        btnEventSave.setVisible(false);
        btnEventEditCancel.setVisible(false);
        cmbCompany.setVisible(false);
        cmbVenue.setVisible(false);
        
        btnVenueDone.setVisible(false);
        btnVenueSave.setVisible(false);
        btnVenueCancel.setVisible(false);
        
        btnTicketDone.setVisible(false);
        btnTicketSave.setVisible(false);
        btnTicketCancel.setVisible(false);
        cmbTicketEvent.setVisible(false);
        cmbTicketUser.setVisible(false);
        cmbTicketType.setVisible(false);
        
        btnUserDone.setVisible(false);
        btnUserSave.setVisible(false);
        btnUserCancel.setVisible(false);
        cmbUserType.setVisible(false);
        
        btnCompanyDone.setVisible(false);
        btnCompanySave.setVisible(false);
        btnCompanyCancel.setVisible(false);
        
        btnSearchVenueClear.setVisible(false);
        btnSearchEventClear.setVisible(false);
        btnSearchUserClear.setVisible(false);
        btnSearchCompanyClear.setVisible(false);

        eventList = new EventList();
        refreshList(lstEventList, eventList);
        
        companyList = new CompanyList();
        refreshComboBox(cmbCompany, companyList);
        refreshList(lstCompanyList, companyList);
        
        venueList = new VenueList();
        refreshComboBox(cmbVenue, venueList);
        refreshList(lstVenueList, venueList);

        
        ticketList = new TicketList();
        refreshList(lstTicketList, ticketList);
        userList = new UserList();
        refreshComboBox(cmbTicketUser, userList);
        refreshComboBox(cmbTicketEvent, eventList);
        refreshList(lstUserList, userList);
        
        loginList = new LoginList();
        addressList = new AddressList();
        
        txtUserPassword.setEditable(false);
        txtUserAddress.setEditable(false);
        txtUserPostCode.setEditable(false);
        
    }
    
    public void refreshList(JList listComponent, IList list) {   
        listModel = new DefaultListModel<>();
        listModel.clear();
        String idString;
            for(IListable currObject : list.getList()) {
                try {
                //idString = String.valueOf(currObject.getID());
                    idString =  currObject.getID() + " - " + currObject.getName();    
                } catch (NullPointerException ex) {
                    idString = "Error retrieving tickets from database";
                }
                listModel.addElement(idString);
            }
        listComponent.setModel(listModel);
    }
  
    private void showPopupMenu(MouseEvent e) {
        eventPopupMenu.show(this, e.getX(), e.getY());
    }
    
    public void refreshComboBox(JComboBox comboBox, IList list) {
        cmbModel = new DefaultComboBoxModel<>();
        cmbModel.removeAllElements();
        String inputString;
        for(IListable currObject: list.getList()) {
            inputString = currObject.getName();
            cmbModel.addElement(inputString);
        }
        comboBox.setModel(cmbModel);
    }
    
    private void refreshAllLists() {
        refreshList(lstEventList, eventList);
        refreshList(lstUserList, userList);
        refreshList(lstTicketList, ticketList);
        refreshList(lstCompanyList, companyList);
        refreshList(lstVenueList, venueList);
        refreshComboBox(cmbVenue, venueList);
        refreshComboBox(cmbCompany, companyList);
        refreshComboBox(cmbTicketUser, userList);
        refreshComboBox(cmbTicketEvent, eventList);
    }
    
    public String addLogin(IListable itemToAdd, ArrayList<String> valuesToAdd) {
        if(itemToAdd.getClass() == new Login().getClass()) {
            itemToAdd = new Login(valuesToAdd.get(0)); 
            String result = this.loginList.addToListString((Login) itemToAdd);
            return result;
        }
        return null;
    }
    
    public String addAddress(IListable itemToAdd, ArrayList<String> valuesToAdd){
        if(itemToAdd.getClass() == new Address().getClass()) {
            itemToAdd = new Address(valuesToAdd.get(0), valuesToAdd.get(1));
            String result = this.addressList.addToListString((Address) itemToAdd);
            return result;
        }   
        return null;
    }
    
    public void setAddEventUI() {
        setEditableEventComponents(true);
        txtEventDescription.setEditable(true);
        clearEventTextFields();
        txtEventDescription.setText("");

        txtEventVenue.setVisible(false);
        txtEventCompany.setVisible(false);

        //cmbCompany.setVisible(true);
        //cmbVenue.setVisible(true);

        btnEventEdit.setVisible(false);
        btnEventDone.setVisible(true);
        btnEventEditCancel.setVisible(true);
        btnEventDelete.setVisible(false);
        btnEventBookTicket.setVisible(false);
    }
    
    public void addEvent() {         
        ArrayList<String> values = new ArrayList<>();
        ArrayList<JTextField> tempList = listEventComponents();

        if(textFieldsAreEmpty(tempList)){
            JOptionPane.showMessageDialog(this, "Please make sure all values are not empty");
        } else {
            int index = cmbCompany.getSelectedIndex();
            Company newCompany = this.companyList.getCompanyList().get(index);
            values.add(String.valueOf(newCompany.getID()));

            index = cmbVenue.getSelectedIndex();
            Venue newVenue = this.venueList.getVenueList().get(index);
            values.add(String.valueOf(newVenue.getID()));

            values.add(txtEventName.getText());
            values.add(txtEventType.getText());
            values.add(txtEventStartTime.getText());
            values.add(txtEventEndTime.getText());
            values.add(txtEventStartDate.getText());
            values.add(txtEventEndDate.getText());
            values.add(txtEventSeatingPrice.getText());
            values.add(txtEventStandingPrice.getText());
            values.add(txtEventAgeRestriction.getText());
            values.add(txtEventDescription.getText());

            addEvent(new Event(), values);

            clearEventTextFields();
            setEditableEventComponents(false);
            
            txtEventDescription.setEditable(false);
            btnEventEdit.setVisible(true);
            btnEventDelete.setVisible(true);
            btnEventDone.setVisible(false);
            btnEventEditCancel.setVisible(false);
            cmbCompany.setVisible(false);
            cmbVenue.setVisible(false);
        }
    }
    
    public void addEvent(IListable itemToAdd, ArrayList<String> valuesToAdd) {
        //this.list.setObject(itemOne, itemTwo, itemThree, itemFour, itemFive, itemSix, itemSeven, itemEight, itemNine);
        //Listable itemToAdd;
        if(itemToAdd.getClass() == new Event().getClass()) {
            itemToAdd = new Event(Integer.parseInt(valuesToAdd.get(0)), Integer.parseInt(valuesToAdd.get(1)), valuesToAdd.get(2), 
                    valuesToAdd.get(3), valuesToAdd.get(4), valuesToAdd.get(5), valuesToAdd.get(6), valuesToAdd.get(7), valuesToAdd.get(8), valuesToAdd.get(9), valuesToAdd.get(10), valuesToAdd.get(11));
            this.eventList.addToList((Event) itemToAdd);
        }
        txtLogHistory.append("ADDED: " + txtEventName.getText() + " to Events!\n");
        refreshList(lstEventList, eventList);
    }
    
    public void setAddVenueUI() {
        btnVenueEdit.setVisible(false);
        btnVenueDelete.setVisible(false);
        btnVenueDone.setVisible(true);
        btnVenueCancel.setVisible(true);
        btnVenueDelete.setVisible(false);

        clearVenueTextFields();
        setEditableVenueComponents(true);
    }
    
    public void addVenue() {
        ArrayList<String> values = new ArrayList<>();

        ArrayList<JTextField> tempList = listVenueComponents();

        if(textFieldsAreEmpty(tempList)){
            JOptionPane.showMessageDialog(this, "Please make sure all values are not empty");
        } else {
            values.add(txtVenueName.getText());
            values.add(txtAddressOne.getText());
            values.add(txtPostcode.getText());
            values.add(txtMaxCapacity.getText());
            values.add(txtSeatedSpaces.getText());
            values.add(txtStandingSpaces.getText());

            addVenue(new Venue(), values);

            clearEventTextFields();
            setEditableVenueComponents(false);

            btnVenueEdit.setVisible(true);
            btnVenueDelete.setVisible(true);
            btnVenueDone.setVisible(false);
            btnVenueCancel.setVisible(false);
        }
    }
    
    public void addVenue(IListable itemToAdd, ArrayList<String> valuesToAdd) {
        if(itemToAdd.getClass() == new Venue().getClass()) {
            itemToAdd = new Venue(valuesToAdd.get(0), valuesToAdd.get(1),  
                    valuesToAdd.get(2), Integer.parseInt(valuesToAdd.get(3)), Integer.parseInt(valuesToAdd.get(4)), Integer.parseInt(valuesToAdd.get(5)));
            this.venueList.addToList((Venue) itemToAdd);
        }
        txtLogHistory.append("ADDED: " + txtVenueName.getText() + " to Venues!\n");
        refreshList(lstVenueList, venueList);
        refreshComboBox(cmbVenue, venueList);
    }
    
    public void setAddTicketUI() {
        setEditableTicketComponents(true);

        clearTicketTextFields();

        //btnAdd.setVisible(false);
        cmbTicketUser.setVisible(true);
        cmbTicketEvent.setVisible(true);
        cmbTicketType.setVisible(true);
        txtTicketType.setVisible(false);
        txtTicketUser.setVisible(false);
        txtTicketEvent.setVisible(false);
        btnTicketEdit.setVisible(false);
        btnTicketDone.setVisible(true);
        btnTicketSave.setVisible(false);
        btnTicketDelete.setVisible(false);
        btnTicketCancel.setVisible(true);
    }
    
    public void addTicket() {
        
        ArrayList<String> values = new ArrayList<>();        
        
        //ArrayList<JTextField> tempList = listTicketComponents();

            int index = cmbTicketUser.getSelectedIndex();
            User newUser = this.userList.getUserList().get(index);
            values.add(String.valueOf(newUser.getID()));

            index = cmbTicketEvent.getSelectedIndex();
            Event newEvent = this.eventList.getEventList().get(index);
            values.add(String.valueOf(newEvent.getID()));

            values.add(cmbTicketType.getSelectedItem().toString());          

            addTicket(new Ticket(), values);

            clearTicketTextFields();
            setEditableTicketComponents(false);
            btnTicketEdit.setVisible(true);
            btnTicketDelete.setVisible(true);
            btnTicketDone.setVisible(false);
            btnTicketCancel.setVisible(false);
            this.cmbTicketUser.setVisible(false);
            this.cmbTicketEvent.setVisible(false);
            this.cmbTicketType.setVisible(false);
            this.txtTicketEvent.setVisible(true);
            this.txtTicketType.setVisible(true);
            this.txtTicketUser.setVisible(true);
    }

    public void addTicket(IListable itemToAdd, ArrayList<String> valuesToAdd) {
        if(itemToAdd.getClass() == new Ticket().getClass()) {
            itemToAdd = new Ticket(Integer.parseInt(valuesToAdd.get(0)), Integer.parseInt(valuesToAdd.get(1)), valuesToAdd.get(2)); 
            this.ticketList.addToList((Ticket) itemToAdd);
            txtLogHistory.append("ADDED: " + itemToAdd.getName() + " to Tickets!\n");  
        }
        refreshList(lstTicketList, ticketList);
    }
    
    public void setAddCompanyUI() {
        btnCompanyDelete.setVisible(false);
        btnCompanyEdit.setVisible(false);
        btnCompanyDone.setVisible(true);
        btnCompanyCancel.setVisible(true);
        clearCompanyTextFields();
        this.setEditableCompanyComponents(true);
    }
    
    public void addCompany() {
        ArrayList<String> values = new ArrayList<>();
        ArrayList<JTextField> tempList = listCompanyComponents();

        if(textFieldsAreEmpty(tempList)){
            JOptionPane.showMessageDialog(this, "Please make sure all values are not empty.");
        } else {
            if(txtCompanyPhoneNumber.getText().length() != 11) {
                JOptionPane.showMessageDialog(this, "Please make sure that the phone number is 11 digits long.");
        } else if(!txtCompanyEmailAddress.getText().contains("@")) {
                JOptionPane.showMessageDialog(this, "Please make sure that the email address contains an @");
            } else {
            values.add(txtCompanyName.getText());
            values.add(this.txtCompanyPhoneNumber.getText());
            values.add(this.txtCompanyEmailAddress.getText());
            
            addCompany(new Company(), values);

            clearCompanyTextFields();
            setEditableCompanyComponents(false);
            btnCompanyEdit.setVisible(true);
            btnCompanyDelete.setVisible(true);
            btnCompanyDone.setVisible(false);
            btnCompanyCancel.setVisible(false);
            }
        }
        //setCompanyTextFields((Company)this.getValue(companyList, lstCompanyList));
    }
    
    public void addCompany(IListable itemToAdd, ArrayList<String> valuesToAdd) {
        if(itemToAdd.getClass() == new Company().getClass()) {
            itemToAdd = new Company(valuesToAdd.get(0), valuesToAdd.get(1), valuesToAdd.get(2)); 
            this.companyList.addToList((Company) itemToAdd);
        }
        txtLogHistory.append("ADDED: " + txtCompanyName.getText() + " to Companies!\n");
        refreshList(lstCompanyList, companyList);
        refreshComboBox(cmbCompany, companyList);
    }
    
        
    public void setAddUserUI() {
        setEditableUserComponents(true);

        clearUserTextFields();
        
        cmbUserType.setVisible(true);
        txtUserType.setVisible(false);
        btnUserEdit.setVisible(false);
        btnUserDelete.setVisible(false);
        btnUserDone.setVisible(true);
        btnUserCancel.setVisible(true);
        btnUserBookTicket.setVisible(false);
    }
        
    public void addUser() {
         
        ArrayList<String> values = new ArrayList<>();
        ArrayList<JTextField> tempList = listUserComponents();
        
        String loginResult = null;
        String addressResult = null;

        if(textFieldsAreEmpty(tempList)){
            JOptionPane.showMessageDialog(this, "Please make sure all values are not empty");
        } else if(txtUserTelephoneNumber.getText().length() != 11) {
                JOptionPane.showMessageDialog(this, "Please make sure that the phone number is 11 digits long.");
        } else if(!txtUserEmailAddress.getText().contains("@")) {
                JOptionPane.showMessageDialog(this, "Please make sure that the email address contains an @");
            } else {
            if(!txtUserPassword.getText().equals("GUEST")){
                values.add(txtUserPassword.getText());
                loginResult = addLogin(new Login(), values);
            }
            values.clear();
            
            if(!txtUserAddress.getText().equals("GUEST")){
                values.add(txtUserAddress.getText());
                values.add(txtUserPostCode.getText());
                addressResult = addAddress(new Address(), values);
            }
            values.clear();
            
            if(null != loginResult) {
                JsonElement j = new JsonParser().parse(loginResult);  
                try {
                    Gson gson = new Gson();
                    Login obj = gson.fromJson(j, Login.class); 
                    values.add(String.valueOf(obj.getID()));
                } catch(Exception ex) {
                    values.add("1");
                }  
            } else {
                values.add("1");
            }
            
            if(null != addressResult) {
                JsonElement j = new JsonParser().parse(addressResult);  
                try {
                    Gson gson = new Gson();
                    Address obj = gson.fromJson(j, Address.class); 
                    values.add(String.valueOf(obj.getID()));
                } catch(Exception ex) {
                    values.add("1");
                }
            } else {
                values.add("1");
            }
            
            values.add(txtUserFirstName.getText());
            values.add(txtUserLastName.getText());
            values.add(txtUserEmailAddress.getText());
            values.add(txtUserDOB.getText());
            values.add(txtUserTelephoneNumber.getText());
            values.add(cmbUserType.getSelectedItem().toString());

            addUser(new User(), values);

            clearUserTextFields();
            setEditableUserComponents(false);
            
            btnUserDone.setVisible(false);
            btnUserCancel.setVisible(false);
            btnUserDelete.setVisible(true);
            btnUserEdit.setVisible(true);
        }
    }
    
    public void addUser(IListable itemToAdd, ArrayList<String> valuesToAdd) { 
        if(itemToAdd.getClass() == new User().getClass()) {
            itemToAdd = new User(Integer.parseInt(valuesToAdd.get(1)), Integer.parseInt(valuesToAdd.get(1)), valuesToAdd.get(2), 
                    valuesToAdd.get(3), valuesToAdd.get(4), valuesToAdd.get(5), valuesToAdd.get(6), valuesToAdd.get(7)); 
            this.userList.addToList((User) itemToAdd);
        }
        txtLogHistory.append("ADDED: " + txtUserFirstName.getText() + " " + txtUserLastName.getText() + " to Users!\n");
        refreshList(lstUserList, userList);
        refreshComboBox(cmbTicketUser, userList);
    }
    
    public void cancelAddEditEvent() {
        setEditableEventComponents(false);
        txtEventDescription.setEditable(false);

        txtEventVenue.setVisible(true);
        txtEventCompany.setVisible(true);

        cmbCompany.setVisible(false);
        cmbVenue.setVisible(false);

        btnEventAdd.setVisible(true);
        btnEventEdit.setVisible(true);
        btnEventDelete.setVisible(true);
        btnEventSave.setVisible(false);
        btnEventDone.setVisible(false);
        btnEventEditCancel.setVisible(false);
        btnEventBookTicket.setVisible(true);  
    }
    
    public void setEditEventUI() {
        setEditableEventComponents(true);
        txtEventDescription.setEditable(true);

        txtEventVenue.setVisible(false);
        txtEventCompany.setVisible(false);
        cmbCompany.setVisible(true);
        cmbVenue.setVisible(true);

        btnEventAdd.setVisible(false);
        btnEventDelete.setVisible(false);
        btnEventSave.setVisible(true);
        btnEventEditCancel.setVisible(true);
        btnEventBookTicket.setVisible(false);

        this.setEditableEventComponents(true);
    }
    
    public void editEvent() {
        this.setEditableEventComponents(false);
        txtEventDescription.setEditable(false);

        btnEventAdd.setVisible(true);
        btnEventDelete.setVisible(true);
        btnEventSave.setVisible(false);
        btnEventEditCancel.setVisible(false);

        Event event = (Event)this.getValue(eventList, lstEventList);

        event.setEventName(txtEventName.getText());
        event.setEventType(txtEventType.getText());

        int index = cmbCompany.getSelectedIndex();
        Company newCompany = this.companyList.getCompanyList().get(index);
        event.setCompanyID(newCompany.getID());

        index = cmbVenue.getSelectedIndex();
        Venue newVenue = this.venueList.getVenueList().get(index);
        event.setVenueID(newVenue.getID());
        
        event.setStartTime(txtEventStartTime.getText());
        event.setEndTime(txtEventEndTime.getText());
        event.setSTARTDATE(txtEventStartDate.getText());
        event.setENDDATE(txtEventEndDate.getText());
        event.setSeatingPrice(txtEventSeatingPrice.getText());
        event.setStandingPrice(txtEventStandingPrice.getText());
        event.setAgeRestriction(txtEventAgeRestriction.getText());
        event.setEVENTDESCRIPTION(txtEventDescription.getText());

        if (eventList.editItem(event)) {
            JOptionPane.showMessageDialog(this, "Changes Saved!");
            txtLogHistory.append("UPDATED: " + txtEventName.getText() + " was updated in Events!\n");
        }

        setEditableEventComponents(false);
        eventList.updateList();
        refreshList(lstEventList, eventList);
        refreshComboBox(cmbTicketEvent, eventList);
    }
   
    public void setEditVenueUI() {
        //btnVenueEdit.setVisible(true);
        btnVenueAdd.setVisible(false);
        btnVenueDelete.setVisible(false);
        btnVenueCancel.setVisible(true);
        btnVenueSave.setVisible(true);

        setEditableVenueComponents(true);
    }
    
    public void cancelAddEditVenue() {
        btnVenueAdd.setVisible(true);
        btnVenueDelete.setVisible(true);
        btnVenueEdit.setVisible(true);
        btnVenueSave.setVisible(false);
        btnVenueDone.setVisible(false);
        btnVenueCancel.setVisible(false);

        setEditableVenueComponents(false);
        clearVenueTextFields();
    }
    
    public void editVenue() {
        setEditableVenueComponents(false);

        btnVenueAdd.setVisible(true);
        btnVenueDelete.setVisible(true);
        btnVenueSave.setVisible(false);
        btnVenueCancel.setVisible(false);

        Venue venue = (Venue)this.getValue(venueList, lstVenueList);

        venue.setVenueName(txtVenueName.getText());
        venue.setAddressOne(txtAddressOne.getText());
        venue.setPostCode(txtPostcode.getText());
        venue.setSeatedSpace(Integer.parseInt(txtSeatedSpaces.getText()));
        venue.setStandingSpace(Integer.parseInt(txtStandingSpaces.getText()));
        venue.setMaximumCapacity();

        if (venueList.editItem(venue)) {
            JOptionPane.showMessageDialog(this, "Changes Saved!");
            txtLogHistory.append("UPDATED: " + txtVenueName.getText() + " was updated in Venues!\n");
        }

        setEditableVenueComponents(false);
        venueList.updateList();
        refreshList(lstVenueList, venueList);
        refreshComboBox(cmbVenue, venueList);
        //setVenueTextFields((Venue)this.getValue(venueList, lstVenueList));  
    }
    
    public void setEditTicketUI() {
        btnTicketAdd.setVisible(false);
        btnTicketDelete.setVisible(false);
        cmbTicketUser.setVisible(true);
        cmbTicketEvent.setVisible(true);
        cmbTicketType.setVisible(true);
        txtTicketEvent.setVisible(false);
        txtTicketUser.setVisible(false);
        txtTicketType.setVisible(false);
        btnTicketEdit.setVisible(true);
        btnTicketSave.setVisible(true);
        btnTicketDone.setVisible(false);
        btnTicketCancel.setVisible(true);

        setEditableTicketComponents(true);
    }
    
    public void cancelAddEditTicket() {
        btnTicketAdd.setVisible(true);
        btnTicketDelete.setVisible(true);
        cmbTicketUser.setVisible(false);
        cmbTicketEvent.setVisible(false);
        cmbTicketType.setVisible(false);
        txtTicketEvent.setVisible(true);
        txtTicketUser.setVisible(true);
        txtTicketType.setVisible(true);
        btnTicketEdit.setVisible(true);
        btnTicketSave.setVisible(false);
        btnTicketDone.setVisible(false);
        btnTicketCancel.setVisible(false);

        setEditableTicketComponents(false);
        clearTicketTextFields();
    }
        
    public void editTicket() {
        this.setEditableTicketComponents(false);

        btnTicketAdd.setVisible(true);
        btnTicketDelete.setVisible(true);
        btnTicketSave.setVisible(false);
        btnTicketCancel.setVisible(false);

        Ticket ticket = (Ticket)this.getValue(ticketList, lstTicketList);

        int index = cmbTicketUser.getSelectedIndex();
        User newUser = this.userList.getUserList().get(index);
        ticket.setUserID(newUser.getID());

        index = cmbTicketEvent.getSelectedIndex();
        Event newEvent = this.eventList.getEventList().get(index);
        ticket.setEventID(newEvent.getID());

        ticket.setTicketType(txtTicketType.getText());

        if (ticketList.editItem(ticket)) {
            JOptionPane.showMessageDialog(this, "Changes Saved!");
            txtLogHistory.append("UPDATED: " + txtTicketUser.getText() + " was updated in Tickets!\n");
        }

        setEditableTicketComponents(false);
        cmbTicketUser.setVisible(false);
        cmbTicketEvent.setVisible(false);
        cmbTicketType.setVisible(false);
        txtTicketEvent.setVisible(true);
        txtTicketUser.setVisible(true);
        txtTicketType.setVisible(true);
        ticketList.updateList();
        refreshList(lstTicketList, ticketList);
        //setTicketTextFields((Ticket)this.getValue(ticketList, lstTicketList));
    }
    
    public void setEditCompanyUI() {
        btnCompanyDelete.setVisible(false);
        btnCompanyAdd.setVisible(false);
        btnCompanySave.setVisible(true);
        btnCompanyCancel.setVisible(true);
        //clearCompanyTextFields();
        this.setEditableCompanyComponents(true);
    }
    
    public void cancelAddEditCompany() {
        btnCompanyDelete.setVisible(true);
        btnCompanyEdit.setVisible(true);
        btnCompanyAdd.setVisible(true);
        btnCompanyDone.setVisible(false);
        btnCompanySave.setVisible(false);
        btnCompanyCancel.setVisible(false);
        clearCompanyTextFields();
        this.setEditableCompanyComponents(false);
    }
    
    public void editCompany() {
        if(txtCompanyPhoneNumber.getText().length() != 11) {
            JOptionPane.showMessageDialog(this, "Please make sure that the phone number is 11 digits long.");
        } else if(!txtCompanyEmailAddress.getText().contains("@")) {
            JOptionPane.showMessageDialog(this, "Please make sure that the email address contains an @");
        } else {
            setEditableCompanyComponents(false);

            btnCompanyAdd.setVisible(true);
            btnCompanyDelete.setVisible(true);
            btnCompanySave.setVisible(false);
            btnCompanyCancel.setVisible(false);

            Company company = (Company)this.getValue(companyList, lstCompanyList);

            company.setCOMPANYNAME(txtCompanyName.getText());
            company.setEMAILADDRESS(txtCompanyEmailAddress.getText());
            company.setPHONENUMBER(txtCompanyPhoneNumber.getText());

            if (companyList.editItem(company)) {
                JOptionPane.showMessageDialog(this, "Changes Saved!");
                txtLogHistory.append("UPDATED: " + txtCompanyName.getText() + " was updated in Companies!\n");
            }

            setEditableCompanyComponents(false);
            companyList.updateList();
            refreshList(lstCompanyList, companyList);
            refreshComboBox(cmbCompany, companyList);
        }
    }
    
        
    public void setEditUserUI() {
        setEditableUserComponents(true);
       
        lstUserList.setEnabled(false);
        txtUserType.setVisible(false);
        cmbUserType.setVisible(true);
        btnUserAdd.setVisible(false);
        btnUserDelete.setVisible(false);
        btnUserSave.setVisible(true);
        btnUserCancel.setVisible(true);
        btnUserBookTicket.setVisible(false);
    } 
        
    public void cancelAddEditUser() {
        setEditableUserComponents(false);

        clearUserTextFields();
        
        txtUserType.setVisible(true);
        cmbUserType.setVisible(false);
        btnUserEdit.setVisible(true);
        btnUserDelete.setVisible(true);
        btnUserAdd.setVisible(true);
        btnUserSave.setVisible(false);
        btnUserDone.setVisible(false);
        btnUserCancel.setVisible(false);
        btnUserBookTicket.setVisible(true);
    }
    public void guestUserValidation() {
        if(cmbUserType.getSelectedItem().toString().equals("GUEST")) {
                txtUserPassword.setEditable(false);
                txtUserPassword.setText("GUEST");
                txtUserAddress.setEditable(false);
                txtUserAddress.setText("GUEST");
                txtUserPostCode.setEditable(false);
                txtUserPostCode.setText("GUEST");
            } else {
               txtUserPassword.setEditable(true);
                //txtUserPassword.setText("");
                txtUserAddress.setEditable(true);
                //txtUserAddress.setText("");
                txtUserPostCode.setEditable(true);
                //txtUserPostCode.setText(""); 

            }
    }
    
    public void editUser() {
        if(txtUserTelephoneNumber.getText().length() != 11) {
                JOptionPane.showMessageDialog(this, "Please make sure that the phone number is 11 digits long.");
        } else if(!txtUserEmailAddress.getText().contains("@")) {
                JOptionPane.showMessageDialog(this, "Please make sure that the email address contains an @");
            } else {
                setEditableUserComponents(false);

                btnUserAdd.setVisible(true);
                btnUserDelete.setVisible(true);
                btnUserSave.setVisible(false);
                btnUserCancel.setVisible(false);

                User user = (User)this.getValue(userList, lstUserList);

                if(user.getADDRESSID() != 1 && !txtUserAddress.getText().equals("GUEST") || !txtUserAddress.getText().equals("")) {
                    user.getUserAddress().setPOSTCODE(txtUserPostCode.getText());
                    user.getUserAddress().setADDRESSLINE(txtUserAddress.getText());
                    addressList.editItem(user.getUserAddress());
                }


                if(user.getLOGINID() != 1 && !txtUserPassword.getText().equals("GUEST") || txtUserPassword.getText().length() > 5) {
                    user.getUserLogin().setPASSWORD(txtUserPassword.getText());
                    loginList.editItem(user.getUserLogin());
                }

                user.setFIRSTNAME(txtUserFirstName.getText());
                user.setLASTNAME(txtUserLastName.getText());
                user.setEMAILADDRESS(txtUserEmailAddress.getText());
                user.setTELEPHONENUMBER(txtUserTelephoneNumber.getText());
                user.setDOB(txtUserDOB.getText());
                user.setUSERTYPE(cmbUserType.getSelectedItem().toString());

                if (userList.editItem(user)) {
                    JOptionPane.showMessageDialog(this, "Changes Saved!");
                    txtLogHistory.append("UPDATED: " + txtUserFirstName.getText() + " " + txtUserLastName.getText() + " was updated in Users!\n");
                } 

                setEditableUserComponents(false);
                userList.updateList();
                refreshList(lstUserList, userList);
                refreshComboBox(cmbTicketUser, userList);
        }
    }
    
    public void deleteEvent() {
        
        IListable event = this.getValue(eventList, lstEventList);
        
        if(null != event) {
            int confirm = JOptionPane.showConfirmDialog(this,
                        "Are you sure you want to delete "
                        + event.getName() + "?",
                        "Confirm deletion", JOptionPane.YES_NO_OPTION);
            if (confirm == JOptionPane.YES_OPTION) {
                txtLogHistory.append("DELETE: " + event.getName() + " from events!\n");
                eventList.deleteItem(event);  
                clearEventTextFields();
                refreshList(lstEventList, eventList);
            }
        }
    }
    
    public void deleteVenue() {    
        IListable venue = this.getValue(venueList, lstVenueList);
        
        if(null != venue) {
            int confirm = JOptionPane.showConfirmDialog(this,
                        "Are you sure you want to delete "
                        + venue.getName() + "?",
                        "Confirm deletion", JOptionPane.YES_NO_OPTION);
            if (confirm == JOptionPane.YES_OPTION) {
                venueList.deleteItem(venue); 
                txtLogHistory.append("DELETED: " + venue.getName() + " from Venues!\n");
                clearVenueTextFields();
                refreshList(lstVenueList, venueList);
            }
        }
    }
    
    public void deleteTicket() {
        
        IListable ticket = this.getValue(ticketList, lstTicketList);
        
        if(null != ticket) {
            int confirm = JOptionPane.showConfirmDialog(this,
                        "Are you sure you want to delete "
                        + ticket.getName() + "?",
                        "Confirm deletion", JOptionPane.YES_NO_OPTION);
            if (confirm == JOptionPane.YES_OPTION) {
                ticketList.deleteItem(ticket); 
                txtLogHistory.append("DELETED: " + ticket.getName() + " from Tickets!\n");
                clearTicketTextFields();
                refreshList(lstTicketList, ticketList);
            }
        }
    }  
        
    public void deleteCompany() {
        IListable company = this.getValue(companyList, lstCompanyList);
        
        if(null != company) {
            int confirm = JOptionPane.showConfirmDialog(this,
                        "Are you sure you want to delete "
                        + company.getName() + "?",
                        "Confirm deletion", JOptionPane.YES_NO_OPTION);
            if (confirm == JOptionPane.YES_OPTION) {
                companyList.deleteItem(company); 
                txtLogHistory.append("DELETED: " + company.getName() + " from Companies!\n");
                clearCompanyTextFields();
                refreshList(lstCompanyList, companyList);
            }
        }
    }
    
    public void deleteUser() {
        
        IListable user = this.getValue(userList, lstUserList);
        
        if(null != user) {
            int confirm = JOptionPane.showConfirmDialog(this,
                        "Are you sure you want to delete "
                        + user.getName() + "?",
                        "Confirm deletion", JOptionPane.YES_NO_OPTION);
            if (confirm == JOptionPane.YES_OPTION) {
                userList.deleteItem(user); 
                txtLogHistory.append("DELETED: " + user.getName() + " from Users!\n");
                clearUserTextFields();
                refreshList(lstUserList, userList);
            }
        }
    }
    
    public void searchEvents() {
        String searchParameter = "name";
        String searchValue = txtEventSearch.getText().toUpperCase();
        
        //this.api.sendGet("Events", parameterAndValues);
        
        this.eventList.updateListWhere(searchParameter, searchValue);
        refreshList(lstEventList, eventList);
        
        this.btnSearchEventClear.setVisible(true);
        this.txtEventSearch.setEditable(false);
    }
    
    public void searchVenues() {
        String searchParameter = "name";
        String searchValue = txtVenueSearch.getText();
        
        //this.api.sendGet("Events", parameterAndValues);
        
        this.venueList.updateListWhere(searchParameter, searchValue);
        refreshList(lstVenueList, venueList);
        
        this.btnSearchVenueClear.setVisible(true);
        this.txtVenueSearch.setEditable(false);
    }
    
    public void searchCompanies() {
        String searchParameter = "name";
        String searchValue = txtCompanySearch.getText();
        
        //this.api.sendGet("Events", parameterAndValues);
        
        this.companyList.updateListWhere(searchParameter, searchValue);
        refreshList(lstCompanyList, companyList);
        
        this.btnSearchCompanyClear.setVisible(true);
        this.txtCompanySearch.setEditable(false);
    }
    
    public void searchUsers() {
        String searchParameter = "id";
        String searchValue = txtUserSearch.getText();
        
        //this.api.sendGet("Events", parameterAndValues);
        
        this.userList.updateListWhere(searchParameter, searchValue);
        refreshList(lstUserList, userList);
        
        this.btnSearchUserClear.setVisible(true);
        this.txtUserSearch.setEditable(false);
    }
    
    public IListable getValue(IList list, JList listComponent) {
        int position = listComponent.getSelectedIndex();
        IListable selectedObject = null;
        if (position == -1){
            JOptionPane.showMessageDialog(this, 
                    "Please select an item",
                    "No selection", JOptionPane.ERROR_MESSAGE);
        } else if(listComponent.isEnabled()) {
            selectedObject = list.getList().get(position);
        }
        return selectedObject;
    }
    
    public void setEventTextFields(Event event) {
        txtEventName.setText(event.getName());
        txtEventType.setText(event.getEventType());
        txtEventSeatingPrice.setText(event.getSeatingPrice());
        txtEventStandingPrice.setText(event.getStandingPrice());
        txtEventStartTime.setText(event.getStartTime());
        txtEventEndTime.setText(event.getEndTime());
        txtEventStartDate.setText(event.getSTARTDATE());
        txtEventEndDate.setText(event.getENDDATE());
        txtEventAgeRestriction.setText(event.getAgeRestriction().substring(0, event.getAgeRestriction().length() - 2));
        cmbCompany.setSelectedItem(event.getCompany().getName());
        txtEventCompany.setText(event.getCompanyName());
        cmbVenue.setSelectedItem(event.getVenue().getName());
        txtEventVenue.setText(event.getVenue().getName());
        txtEventDescription.setText(event.getEVENTDESCRIPTION());
    }
    
    public void setVenueTextFields(Venue venue) {
        txtVenueName.setText(venue.getName());
        txtAddressOne.setText(venue.getADDRESSLINE1());
        txtPostcode.setText(venue.getPOSTCODE());
        txtMaxCapacity.setText(Integer.toString(venue.getMAXIMUMCAPACITY()));
        txtSeatedSpaces.setText(Integer.toString(venue.getSEATEDSPACES()));
        txtStandingSpaces.setText(Integer.toString(venue.getSTANDINGSPACES()));
    }
    
    public void setTicketTextFields(Ticket ticket) {
        txtTicketUser.setText(ticket.getUser().getFIRSTNAME() + " " + ticket.getUser().getLASTNAME());
        cmbTicketUser.setSelectedItem(ticket.getUser().getName());
        txtTicketEvent.setText(ticket.getEvent().getName());
        cmbTicketEvent.setSelectedItem(ticket.getEvent().getName());
        cmbTicketType.setSelectedItem(ticket.getTicketType());
        txtTicketType.setText(ticket.getTicketType());
    }
    
    public void setCompanyTextFields(Company company) {
        txtCompanyName.setText(company.getName());
        txtCompanyEmailAddress.setText(company.getEMAILADDRESS());
        txtCompanyPhoneNumber.setText(company.getPHONENUMBER());
    }
    
    public void setUserTextFields(User user) {
        txtUserFirstName.setText(user.getFIRSTNAME());
        txtUserLastName.setText(user.getLASTNAME());
        txtUserAddress.setText(user.getUserAddress().getName());
        txtUserDOB.setText(user.getDOB());
        txtUserEmailAddress.setText(user.getEMAILADDRESS());
        txtUserPassword.setText(user.getUserLogin().getName());
        txtUserPostCode.setText(user.getUserAddress().getPOSTCODE());
        txtUserTelephoneNumber.setText(user.getTELEPHONENUMBER());
        txtUserType.setText(user.getUSERTYPE());
        cmbUserType.setSelectedItem(user.getUSERTYPE());
    }
    
    public void setEditableEventComponents(Boolean b) {
        ArrayList<JTextField> textFieldList = listEventComponents();
        
        cmbCompany.setVisible(b);
        cmbVenue.setVisible(b);
        txtEventCompany.setVisible(!b);
        txtEventVenue.setVisible(!b);
        setListComponentsEditable(b, textFieldList);
        lstEventList.setEnabled(!b);
    }
    
    public void setEditableVenueComponents(Boolean b) {     
        ArrayList<JTextField> textFieldList = listVenueComponents();
        
        setListComponentsEditable(b, textFieldList);
        
        txtMaxCapacity.setEditable(false);
        lstVenueList.setEnabled(!b);
    }
    
    public void setEditableCompanyComponents(Boolean b) {     
        ArrayList<JTextField> textFieldList = listCompanyComponents();
        
        setListComponentsEditable(b, textFieldList);
        
        lstCompanyList.setEnabled(!b);
    }
    
    public void setEditableUserComponents(Boolean b) {     
        ArrayList<JTextField> textFieldList = listUserComponents();
        
        setListComponentsEditable(b, textFieldList);
        
        lstUserList.setEnabled(!b);
    }
    
    public void setEditableTicketComponents(Boolean b) {
//        txtTicketUser.setEditable(b);
//        txtTicketEvent.setEditable(b);
//        txtTicketType.setEditable(b);
        lstTicketList.setEnabled(!b);
    }
 
    public void setListComponentsEditable(Boolean b, ArrayList<JTextField> textFieldList) {
        for(JTextField currField : textFieldList) {
            currField.setEditable(b);
        }   
    }
    
    public ArrayList<JTextField> listEventComponents() {
        ArrayList<JTextField> components = new ArrayList<>();
        
        components.add(txtEventName);
        components.add(txtEventType);
        components.add(txtEventAgeRestriction);
        components.add(txtEventStartTime);
        components.add(txtEventEndTime);
        components.add(txtEventStartDate);
        components.add(txtEventEndDate);
        components.add(txtEventSeatingPrice);
        components.add(txtEventStandingPrice);
        
        return components;
    }
    
    public ArrayList<JTextField> listVenueComponents() {
        ArrayList<JTextField> components = new ArrayList<>();
        
        components.add(txtVenueName);
        components.add(txtAddressOne);
        components.add(txtPostcode);
        components.add(txtMaxCapacity);
        components.add(txtSeatedSpaces);
        components.add(txtStandingSpaces);
        
        return components;
    }
    
    public ArrayList<JTextField> listTicketComponents() {
        ArrayList<JTextField> components = new ArrayList<>();
 
        components.add(txtTicketUser);        
        components.add(txtTicketEvent);
        components.add(txtTicketType);        
        
        return components;
    }
    
    public ArrayList<JTextField> listCompanyComponents() {
        ArrayList<JTextField> components = new ArrayList<>();
 
        components.add(txtCompanyName);        
        components.add(txtCompanyEmailAddress);
        components.add(txtCompanyPhoneNumber);
        
        return components;
    }
    
    public ArrayList<JTextField> listUserComponents() {
        ArrayList<JTextField> components = new ArrayList<>();

        components.add(txtUserFirstName);
        components.add(txtUserLastName);
        components.add(txtUserAddress);
        components.add(txtUserDOB);
        components.add(txtUserEmailAddress);
        components.add(txtUserPassword);
        components.add(txtUserPostCode);
        components.add(txtUserTelephoneNumber);
        //components.add(txtUserType);

        return components;
    }
    
    public Boolean textFieldsAreEmpty(ArrayList<JTextField> arrayList) {
        Boolean result = false;
        ArrayList<JTextField> components = arrayList;
        
        for(JTextField currTxt : components) {
            if(currTxt.getText().equals("")) {
                result = true;
            }
        }
        return result;
    }
    public void clearEventTextFields() {
        txtEventName.setText("");
        txtEventType.setText("");
        txtEventCompany.setText("");
        txtEventVenue.setText("");
        txtEventAgeRestriction.setText("");
        txtEventStartTime.setText("");
        txtEventEndTime.setText("");
        txtEventStartDate.setText("");
        txtEventEndDate.setText("");
        txtEventSeatingPrice.setText("");
        txtEventStandingPrice.setText("");
        txtEventDescription.setText("");
    }
    
    public void clearVenueTextFields() {
        txtVenueName.setText("");
        txtAddressOne.setText("");
        txtPostcode.setText("");
        txtMaxCapacity.setText("");
        txtSeatedSpaces.setText("");
        txtStandingSpaces.setText("");
    }
    
    public void clearTicketTextFields() {
        txtTicketUser.setText("");
        txtTicketEvent.setText("");
        txtTicketType.setText("");
    }
    
    public void clearCompanyTextFields() {
        txtCompanyName.setText("");
        txtCompanyEmailAddress.setText("");
        txtCompanyPhoneNumber.setText("");
    }
    
    public void clearUserTextFields() {
        txtUserFirstName.setText("");
        txtUserLastName.setText("");
        txtUserAddress.setText("");
        txtUserDOB.setText("");
        txtUserEmailAddress.setText("");
        txtUserPassword.setText("");
        txtUserPostCode.setText("");
        txtUserTelephoneNumber.setText("");
        txtUserType.setText("");
    }
    
    public void calculateMaxSpaces() {
        int value3 = 0;
        
        try {
            int value1 = Integer.parseInt(txtStandingSpaces.getText());
            int value2 = Integer.parseInt(txtSeatedSpaces.getText());

            value3 = value1 + value2;
        } catch(NumberFormatException ex) {
            
        }
        txtMaxCapacity.setText(String.valueOf(value3));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        eventPopupMenu = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jButton1 = new javax.swing.JButton();
        tabPane = new javax.swing.JTabbedPane();
        jPanelHome = new javax.swing.JPanel();
        btnGoToEvents = new javax.swing.JButton();
        btnGoToVenues = new javax.swing.JButton();
        btnGoToTickets = new javax.swing.JButton();
        btnGoToCompanies = new javax.swing.JButton();
        btnGoToUsers = new javax.swing.JButton();
        jPanelEvents = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstEventList = new javax.swing.JList();
        txtEventSearch = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        editEventsPanel = new javax.swing.JPanel();
        txtEventName = new javax.swing.JTextField();
        btnEventSave = new javax.swing.JButton();
        btnEventEditCancel = new javax.swing.JButton();
        cmbVenue = new javax.swing.JComboBox();
        cmbCompany = new javax.swing.JComboBox();
        lblEventType = new javax.swing.JLabel();
        lblSeatingPrice = new javax.swing.JLabel();
        lblStandingPrice = new javax.swing.JLabel();
        lblAgeRestriction = new javax.swing.JLabel();
        txtEventType = new javax.swing.JTextField();
        txtEventSeatingPrice = new javax.swing.JTextField();
        txtEventStandingPrice = new javax.swing.JTextField();
        lblSeating = new javax.swing.JLabel();
        txtEventAgeRestriction = new javax.swing.JTextField();
        btnEventDone = new javax.swing.JButton();
        txtEventStartTime = new javax.swing.JTextField();
        txtEventEndTime = new javax.swing.JTextField();
        lblStartTime = new javax.swing.JLabel();
        lblEndTime = new javax.swing.JLabel();
        lblCompany = new javax.swing.JLabel();
        eventNameLbl = new javax.swing.JLabel();
        venueLbl = new javax.swing.JLabel();
        txtEventVenue = new javax.swing.JTextField();
        txtEventCompany = new javax.swing.JTextField();
        lblEndDate = new javax.swing.JLabel();
        lblStartDate = new javax.swing.JLabel();
        txtEventStartDate = new javax.swing.JTextField();
        txtEventEndDate = new javax.swing.JTextField();
        lblEventDescription = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtEventDescription = new javax.swing.JTextArea();
        btnEventBookTicket = new javax.swing.JButton();
        btnEventAdd = new javax.swing.JButton();
        btnEventEdit = new javax.swing.JButton();
        btnEventDelete = new javax.swing.JButton();
        btnSearchEventClear = new javax.swing.JButton();
        btnEventHome = new javax.swing.JButton();
        jPanelVenue = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        lstVenueList = new javax.swing.JList();
        btnVenueAdd = new javax.swing.JButton();
        btnVenueEdit = new javax.swing.JButton();
        btnVenueDelete = new javax.swing.JButton();
        txtVenueSearch = new javax.swing.JTextField();
        btnVenueSearch = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        txtVenueName = new javax.swing.JTextField();
        txtPostcode = new javax.swing.JTextField();
        txtMaxCapacity = new javax.swing.JTextField();
        txtStandingSpaces = new javax.swing.JTextField();
        btnVenueDone = new javax.swing.JButton();
        btnVenueSave = new javax.swing.JButton();
        btnVenueCancel = new javax.swing.JButton();
        lblStandingSpaces = new javax.swing.JLabel();
        lblMaxCapacity = new javax.swing.JLabel();
        lblPostCode = new javax.swing.JLabel();
        lblVenueName = new javax.swing.JLabel();
        txtSeatedSpaces = new javax.swing.JTextField();
        txtAddressOne = new javax.swing.JTextField();
        lblAddressOne = new javax.swing.JLabel();
        lblSeatingVenue = new javax.swing.JLabel();
        lblSeatedSpaces = new javax.swing.JLabel();
        btnSearchVenueClear = new javax.swing.JButton();
        btnVenueHome = new javax.swing.JButton();
        jPanelTickets = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        lstTicketList = new javax.swing.JList();
        btnTicketAdd = new javax.swing.JButton();
        btnTicketEdit = new javax.swing.JButton();
        btnTicketDelete = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        btnTicketDone = new javax.swing.JButton();
        btnTicketCancel = new javax.swing.JButton();
        btnTicketSave = new javax.swing.JButton();
        lblTicketUser = new javax.swing.JLabel();
        lblTicketEvent = new javax.swing.JLabel();
        txtTicketUser = new javax.swing.JTextField();
        txtTicketEvent = new javax.swing.JTextField();
        txtTicketType = new javax.swing.JTextField();
        cmbTicketUser = new javax.swing.JComboBox();
        cmbTicketEvent = new javax.swing.JComboBox();
        lblTicketType = new javax.swing.JLabel();
        cmbTicketType = new javax.swing.JComboBox();
        btnTicketHome = new javax.swing.JButton();
        jPanelCompanies = new javax.swing.JPanel();
        btnCompanySearch = new javax.swing.JButton();
        txtCompanySearch = new javax.swing.JTextField();
        btnSearchCompanyClear = new javax.swing.JButton();
        jScrollPane9 = new javax.swing.JScrollPane();
        lstCompanyList = new javax.swing.JList();
        btnCompanyDelete = new javax.swing.JButton();
        btnCompanyEdit = new javax.swing.JButton();
        btnCompanyAdd = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lblCompanyName = new javax.swing.JLabel();
        txtCompanyName = new javax.swing.JTextField();
        txtCompanyEmailAddress = new javax.swing.JTextField();
        txtCompanyPhoneNumber = new javax.swing.JTextField();
        lblEmailAddress = new javax.swing.JLabel();
        lblPhoneNumber = new javax.swing.JLabel();
        btnCompanyDone = new javax.swing.JButton();
        btnCompanySave = new javax.swing.JButton();
        btnCompanyCancel = new javax.swing.JButton();
        btnCompaniesHome = new javax.swing.JButton();
        jPanelUsers = new javax.swing.JPanel();
        btnUserSearch = new javax.swing.JButton();
        txtUserSearch = new javax.swing.JTextField();
        btnSearchUserClear = new javax.swing.JButton();
        jScrollPane10 = new javax.swing.JScrollPane();
        lstUserList = new javax.swing.JList();
        btnUserDelete = new javax.swing.JButton();
        btnUserEdit = new javax.swing.JButton();
        btnUserAdd = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        lblUserFirstName = new javax.swing.JLabel();
        txtUserFirstName = new javax.swing.JTextField();
        lblUserLastName = new javax.swing.JLabel();
        txtUserLastName = new javax.swing.JTextField();
        lblUserDOB = new javax.swing.JLabel();
        lblUserTelephoneNumber = new javax.swing.JLabel();
        lblUserEmailAddress = new javax.swing.JLabel();
        lblUserType = new javax.swing.JLabel();
        lblUserPassword = new javax.swing.JLabel();
        lblUserAddressTitle = new javax.swing.JLabel();
        lblUserAddress = new javax.swing.JLabel();
        lblUserPostCode = new javax.swing.JLabel();
        txtUserTelephoneNumber = new javax.swing.JTextField();
        txtUserDOB = new javax.swing.JTextField();
        txtUserEmailAddress = new javax.swing.JTextField();
        txtUserPassword = new javax.swing.JTextField();
        txtUserAddress = new javax.swing.JTextField();
        txtUserPostCode = new javax.swing.JTextField();
        btnUserDone = new javax.swing.JButton();
        btnUserSave = new javax.swing.JButton();
        btnUserCancel = new javax.swing.JButton();
        btnUserBookTicket = new javax.swing.JButton();
        cmbUserType = new javax.swing.JComboBox();
        txtUserType = new javax.swing.JTextField();
        btnUsersHome = new javax.swing.JButton();
        eventSystemLabel = new javax.swing.JLabel();
        lblLiveHelp = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtLiveHelp = new javax.swing.JTextArea();
        btnExit = new javax.swing.JButton();
        lblLogHistory = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtLogHistory = new javax.swing.JTextArea();
        btnLogHistoryClear = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();

        eventPopupMenu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                eventPopupMenuMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                eventPopupMenuMouseReleased(evt);
            }
        });

        jMenuItem1.setText("jMenuItem1");
        eventPopupMenu.add(jMenuItem1);

        jButton1.setText("jButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(35, 62, 104));
        setUndecorated(true);
        setResizable(false);

        tabPane.setBackground(new java.awt.Color(35, 62, 104));

        jPanelHome.setBackground(new java.awt.Color(255, 255, 255));

        btnGoToEvents.setBackground(new java.awt.Color(35, 62, 104));
        btnGoToEvents.setForeground(new java.awt.Color(255, 255, 255));
        btnGoToEvents.setText("View Events");
        btnGoToEvents.setToolTipText("View events");
        btnGoToEvents.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnGoToEventsMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGoToEventsMouseEntered(evt);
            }
        });
        btnGoToEvents.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoToEventsActionPerformed(evt);
            }
        });

        btnGoToVenues.setBackground(new java.awt.Color(35, 62, 104));
        btnGoToVenues.setForeground(new java.awt.Color(255, 255, 255));
        btnGoToVenues.setText("View Venues");
        btnGoToVenues.setToolTipText("View venues");
        btnGoToVenues.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnGoToVenuesMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGoToVenuesMouseEntered(evt);
            }
        });
        btnGoToVenues.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoToVenuesActionPerformed(evt);
            }
        });

        btnGoToTickets.setBackground(new java.awt.Color(35, 62, 104));
        btnGoToTickets.setForeground(new java.awt.Color(255, 255, 255));
        btnGoToTickets.setText("View Tickets");
        btnGoToTickets.setToolTipText("View tickets");
        btnGoToTickets.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnGoToTicketsMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGoToTicketsMouseEntered(evt);
            }
        });
        btnGoToTickets.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoToTicketsActionPerformed(evt);
            }
        });

        btnGoToCompanies.setBackground(new java.awt.Color(35, 62, 104));
        btnGoToCompanies.setForeground(new java.awt.Color(255, 255, 255));
        btnGoToCompanies.setText("View Companies");
        btnGoToCompanies.setToolTipText("View companies");
        btnGoToCompanies.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnGoToCompaniesMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGoToCompaniesMouseEntered(evt);
            }
        });
        btnGoToCompanies.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoToCompaniesActionPerformed(evt);
            }
        });

        btnGoToUsers.setBackground(new java.awt.Color(35, 62, 104));
        btnGoToUsers.setForeground(new java.awt.Color(255, 255, 255));
        btnGoToUsers.setText("View Users");
        btnGoToUsers.setToolTipText("View users");
        btnGoToUsers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnGoToUsersMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGoToUsersMouseEntered(evt);
            }
        });
        btnGoToUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoToUsersActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelHomeLayout = new javax.swing.GroupLayout(jPanelHome);
        jPanelHome.setLayout(jPanelHomeLayout);
        jPanelHomeLayout.setHorizontalGroup(
            jPanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelHomeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnGoToEvents, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnGoToVenues, javax.swing.GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
                    .addComponent(btnGoToTickets, javax.swing.GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
                    .addComponent(btnGoToCompanies, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
                    .addComponent(btnGoToUsers, javax.swing.GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE))
                .addGap(638, 638, 638))
        );
        jPanelHomeLayout.setVerticalGroup(
            jPanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelHomeLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(btnGoToEvents, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGoToVenues, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGoToTickets, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGoToCompanies, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGoToUsers, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(302, Short.MAX_VALUE))
        );

        btnGoToEvents.setContentAreaFilled(false);
        btnGoToEvents.setOpaque(true);
        btnGoToVenues.setContentAreaFilled(false);
        btnGoToVenues.setOpaque(true);
        btnGoToTickets.setContentAreaFilled(false);
        btnGoToTickets.setOpaque(true);
        btnGoToCompanies.setContentAreaFilled(false);
        btnGoToCompanies.setOpaque(true);
        btnGoToUsers.setContentAreaFilled(false);
        btnGoToUsers.setOpaque(true);

        tabPane.addTab("", new javax.swing.ImageIcon(getClass().getResource("/Icons/HomeIcon.png")), jPanelHome, "Home"); // NOI18N

        jPanelEvents.setBackground(new java.awt.Color(255, 255, 255));
        jPanelEvents.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanelEventsMouseEntered(evt);
            }
        });

        lstEventList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "ACDC Goes Live", "Justin Bieber World Tour" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        lstEventList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstEventListMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lstEventListMouseEntered(evt);
            }
        });
        jScrollPane1.setViewportView(lstEventList);

        txtEventSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtEventSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventSearchMouseEntered(evt);
            }
        });

        btnSearch.setBackground(new java.awt.Color(35, 62, 104));
        btnSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_search_white_24dp_1x.png"))); // NOI18N
        btnSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSearchMouseEntered(evt);
            }
        });
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        editEventsPanel.setBackground(new java.awt.Color(255, 255, 255));

        txtEventName.setEditable(false);
        txtEventName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventNameMouseEntered(evt);
            }
        });

        btnEventSave.setBackground(new java.awt.Color(35, 62, 104));
        btnEventSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_save_white_18dp_2x.png"))); // NOI18N
        btnEventSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEventSaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEventSaveMouseEntered(evt);
            }
        });
        btnEventSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEventSaveActionPerformed(evt);
            }
        });

        btnEventEditCancel.setBackground(new java.awt.Color(35, 62, 104));
        btnEventEditCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_block_white_18dp_2x.png"))); // NOI18N
        btnEventEditCancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEventEditCancelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEventEditCancelMouseEntered(evt);
            }
        });
        btnEventEditCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEventEditCancelActionPerformed(evt);
            }
        });

        cmbVenue.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        cmbCompany.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbCompany.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCompanyActionPerformed(evt);
            }
        });

        lblEventType.setBackground(new java.awt.Color(255, 255, 255));
        lblEventType.setForeground(new java.awt.Color(45, 80, 134));
        lblEventType.setText("Event Type:");

        lblSeatingPrice.setBackground(new java.awt.Color(255, 255, 255));
        lblSeatingPrice.setForeground(new java.awt.Color(45, 80, 134));
        lblSeatingPrice.setText("Seating Price:");

        lblStandingPrice.setBackground(new java.awt.Color(255, 255, 255));
        lblStandingPrice.setForeground(new java.awt.Color(45, 80, 134));
        lblStandingPrice.setText("Standing Price:");

        lblAgeRestriction.setBackground(new java.awt.Color(255, 255, 255));
        lblAgeRestriction.setForeground(new java.awt.Color(45, 80, 134));
        lblAgeRestriction.setText("Age Restriction:");

        txtEventType.setEditable(false);
        txtEventType.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventTypeMouseEntered(evt);
            }
        });

        txtEventSeatingPrice.setEditable(false);
        txtEventSeatingPrice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventSeatingPriceMouseEntered(evt);
            }
        });
        txtEventSeatingPrice.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtEventSeatingPriceKeyReleased(evt);
            }
        });

        txtEventStandingPrice.setEditable(false);
        txtEventStandingPrice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventStandingPriceMouseEntered(evt);
            }
        });
        txtEventStandingPrice.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtEventStandingPriceKeyReleased(evt);
            }
        });

        lblSeating.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lblSeating.setForeground(new java.awt.Color(45, 80, 134));
        lblSeating.setText("Seating");

        txtEventAgeRestriction.setEditable(false);
        txtEventAgeRestriction.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtEventAgeRestriction.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventAgeRestrictionMouseEntered(evt);
            }
        });

        btnEventDone.setBackground(new java.awt.Color(35, 62, 104));
        btnEventDone.setForeground(new java.awt.Color(255, 255, 255));
        btnEventDone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_done_white_18dp_2x.png"))); // NOI18N
        btnEventDone.setToolTipText("Add new Item");
        btnEventDone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEventDoneMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEventDoneMouseEntered(evt);
            }
        });
        btnEventDone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEventDoneActionPerformed(evt);
            }
        });

        txtEventStartTime.setEditable(false);
        txtEventStartTime.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventStartTimeMouseEntered(evt);
            }
        });

        txtEventEndTime.setEditable(false);
        txtEventEndTime.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventEndTimeMouseEntered(evt);
            }
        });

        lblStartTime.setBackground(new java.awt.Color(255, 255, 255));
        lblStartTime.setForeground(new java.awt.Color(45, 80, 134));
        lblStartTime.setText("Start Time:");

        lblEndTime.setBackground(new java.awt.Color(255, 255, 255));
        lblEndTime.setForeground(new java.awt.Color(45, 80, 134));
        lblEndTime.setText("End Time:");

        lblCompany.setBackground(new java.awt.Color(255, 255, 255));
        lblCompany.setForeground(new java.awt.Color(45, 80, 134));
        lblCompany.setText("Company:");

        eventNameLbl.setBackground(new java.awt.Color(255, 255, 255));
        eventNameLbl.setForeground(new java.awt.Color(45, 80, 134));
        eventNameLbl.setText("Event Name:");

        venueLbl.setBackground(new java.awt.Color(255, 255, 255));
        venueLbl.setForeground(new java.awt.Color(45, 80, 134));
        venueLbl.setText("Venue:");

        txtEventVenue.setEditable(false);
        txtEventVenue.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventVenueMouseEntered(evt);
            }
        });

        txtEventCompany.setEditable(false);
        txtEventCompany.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventCompanyMouseEntered(evt);
            }
        });

        lblEndDate.setBackground(new java.awt.Color(255, 255, 255));
        lblEndDate.setForeground(new java.awt.Color(45, 80, 134));
        lblEndDate.setText("End Date:");

        lblStartDate.setBackground(new java.awt.Color(255, 255, 255));
        lblStartDate.setForeground(new java.awt.Color(45, 80, 134));
        lblStartDate.setText("Start Date:");

        txtEventStartDate.setEditable(false);
        txtEventStartDate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventStartDateMouseEntered(evt);
            }
        });

        txtEventEndDate.setEditable(false);
        txtEventEndDate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventEndDateMouseEntered(evt);
            }
        });

        lblEventDescription.setBackground(new java.awt.Color(255, 255, 255));
        lblEventDescription.setForeground(new java.awt.Color(45, 80, 134));
        lblEventDescription.setText("Description:");

        txtEventDescription.setEditable(false);
        txtEventDescription.setColumns(20);
        txtEventDescription.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtEventDescription.setLineWrap(true);
        txtEventDescription.setRows(5);
        txtEventDescription.setWrapStyleWord(true);
        txtEventDescription.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtEventDescriptionMouseEntered(evt);
            }
        });
        jScrollPane6.setViewportView(txtEventDescription);

        btnEventBookTicket.setBackground(new java.awt.Color(35, 62, 104));
        btnEventBookTicket.setForeground(new java.awt.Color(255, 255, 255));
        btnEventBookTicket.setText("Book ticket for this event");
        btnEventBookTicket.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEventBookTicketMouseEntered(evt);
            }
        });
        btnEventBookTicket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEventBookTicketActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout editEventsPanelLayout = new javax.swing.GroupLayout(editEventsPanel);
        editEventsPanel.setLayout(editEventsPanelLayout);
        editEventsPanelLayout.setHorizontalGroup(
            editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editEventsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(editEventsPanelLayout.createSequentialGroup()
                        .addComponent(btnEventSave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEventDone)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEventEditCancel))
                    .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblStartTime)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, editEventsPanelLayout.createSequentialGroup()
                            .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblAgeRestriction)
                                .addComponent(lblEndDate)
                                .addComponent(lblStartDate)
                                .addComponent(lblEndTime)
                                .addComponent(lblEventType)
                                .addComponent(lblCompany)
                                .addComponent(venueLbl)
                                .addComponent(eventNameLbl)
                                .addComponent(lblEventDescription))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                            .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtEventStartTime, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtEventEndTime, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtEventStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtEventEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addComponent(txtEventName, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(editEventsPanelLayout.createSequentialGroup()
                                    .addComponent(txtEventVenue, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(cmbVenue, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtEventType, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(txtEventAgeRestriction, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(editEventsPanelLayout.createSequentialGroup()
                                    .addComponent(txtEventCompany, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(cmbCompany, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addComponent(lblSeating)
                    .addGroup(editEventsPanelLayout.createSequentialGroup()
                        .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblStandingPrice)
                            .addComponent(lblSeatingPrice))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtEventStandingPrice, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                            .addComponent(txtEventSeatingPrice, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                            .addComponent(btnEventBookTicket, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        editEventsPanelLayout.setVerticalGroup(
            editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editEventsPanelLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtEventName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(eventNameLbl))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(venueLbl)
                    .addComponent(txtEventVenue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbVenue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCompany)
                    .addComponent(txtEventCompany, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbCompany, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEventType)
                    .addComponent(txtEventType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblEventDescription)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblStartTime)
                    .addGroup(editEventsPanelLayout.createSequentialGroup()
                        .addComponent(txtEventStartTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEventEndTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblEndTime))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEventStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblStartDate))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEventEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblEndDate))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEventAgeRestriction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblAgeRestriction))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSeating, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblStandingPrice)
                    .addGroup(editEventsPanelLayout.createSequentialGroup()
                        .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEventSeatingPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblSeatingPrice))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtEventStandingPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEventBookTicket, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(editEventsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnEventDone, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEventSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEventEditCancel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        btnEventSave.setContentAreaFilled(false);
        btnEventSave.setOpaque(true);
        btnEventEditCancel.setContentAreaFilled(false);
        btnEventEditCancel.setOpaque(true);
        btnEventDone.setContentAreaFilled(false);
        btnEventDone.setOpaque(true);
        btnEventBookTicket.setContentAreaFilled(false);
        btnEventBookTicket.setOpaque(true);

        btnEventAdd.setBackground(new java.awt.Color(35, 62, 104));
        btnEventAdd.setForeground(new java.awt.Color(255, 255, 255));
        btnEventAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_add_white_18dp_2x.png"))); // NOI18N
        btnEventAdd.setToolTipText("Add new Item");
        btnEventAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEventAddMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEventAddMouseEntered(evt);
            }
        });
        btnEventAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEventAddActionPerformed(evt);
            }
        });

        btnEventEdit.setBackground(new java.awt.Color(35, 62, 104));
        btnEventEdit.setForeground(new java.awt.Color(255, 255, 255));
        btnEventEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_create_white_18dp_2x.png"))); // NOI18N
        btnEventEdit.setToolTipText("Edit item");
        btnEventEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEventEditMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEventEditMouseEntered(evt);
            }
        });
        btnEventEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEventEditActionPerformed(evt);
            }
        });

        btnEventDelete.setBackground(new java.awt.Color(35, 62, 104));
        btnEventDelete.setForeground(new java.awt.Color(255, 255, 255));
        btnEventDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_clear_white_18dp_2x.png"))); // NOI18N
        btnEventDelete.setToolTipText("Delete Item");
        btnEventDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEventDeleteMouseEntered(evt);
            }
        });
        btnEventDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEventDeleteActionPerformed(evt);
            }
        });

        btnSearchEventClear.setBackground(new java.awt.Color(45, 80, 134));
        btnSearchEventClear.setForeground(new java.awt.Color(255, 255, 255));
        btnSearchEventClear.setText("x");
        btnSearchEventClear.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSearchEventClear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSearchEventClearMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSearchEventClearMouseExited(evt);
            }
        });
        btnSearchEventClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchEventClearActionPerformed(evt);
            }
        });

        btnEventHome.setBackground(new java.awt.Color(35, 62, 104));
        btnEventHome.setForeground(new java.awt.Color(255, 255, 255));
        btnEventHome.setText("Back to Homescreen");
        btnEventHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEventHomeMouseEntered(evt);
            }
        });
        btnEventHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEventHomeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelEventsLayout = new javax.swing.GroupLayout(jPanelEvents);
        jPanelEvents.setLayout(jPanelEventsLayout);
        jPanelEventsLayout.setHorizontalGroup(
            jPanelEventsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEventsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelEventsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelEventsLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelEventsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelEventsLayout.createSequentialGroup()
                                .addGroup(jPanelEventsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btnEventAdd)
                                    .addComponent(btnEventEdit, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnEventDelete, javax.swing.GroupLayout.Alignment.LEADING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(editEventsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelEventsLayout.createSequentialGroup()
                                .addGap(461, 461, 461)
                                .addComponent(btnEventHome))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelEventsLayout.createSequentialGroup()
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtEventSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearchEventClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelEventsLayout.setVerticalGroup(
            jPanelEventsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEventsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelEventsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelEventsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtEventSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSearchEventClear, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelEventsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanelEventsLayout.createSequentialGroup()
                        .addGroup(jPanelEventsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelEventsLayout.createSequentialGroup()
                                .addComponent(btnEventAdd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnEventEdit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnEventDelete))
                            .addComponent(editEventsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 474, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                        .addComponent(btnEventHome)))
                .addContainerGap())
        );

        btnSearch.setContentAreaFilled(false);
        btnSearch.setOpaque(true);
        btnEventAdd.setContentAreaFilled(false);
        btnEventAdd.setOpaque(true);
        btnEventEdit.setContentAreaFilled(false);
        btnEventEdit.setOpaque(true);
        btnEventDelete.setContentAreaFilled(false);
        btnEventDelete.setOpaque(true);
        btnSearchEventClear.setContentAreaFilled(false);
        btnSearchEventClear.setOpaque(true);
        btnEventHome.setContentAreaFilled(false);
        btnEventHome.setOpaque(true);

        tabPane.addTab("", new javax.swing.ImageIcon(getClass().getResource("/Icons/EventIcon.png")), jPanelEvents); // NOI18N

        jPanelVenue.setBackground(new java.awt.Color(255, 255, 255));
        jPanelVenue.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanelVenueMouseEntered(evt);
            }
        });

        lstVenueList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "ACDC Goes Live", "Justin Bieber World Tour" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        lstVenueList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstVenueListMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lstVenueListMouseEntered(evt);
            }
        });
        jScrollPane3.setViewportView(lstVenueList);

        btnVenueAdd.setBackground(new java.awt.Color(35, 62, 104));
        btnVenueAdd.setForeground(new java.awt.Color(255, 255, 255));
        btnVenueAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_add_white_18dp_2x.png"))); // NOI18N
        btnVenueAdd.setToolTipText("Add new Item");
        btnVenueAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnVenueAddMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnVenueAddMouseEntered(evt);
            }
        });
        btnVenueAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVenueAddActionPerformed(evt);
            }
        });

        btnVenueEdit.setBackground(new java.awt.Color(35, 62, 104));
        btnVenueEdit.setForeground(new java.awt.Color(255, 255, 255));
        btnVenueEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_create_white_18dp_2x.png"))); // NOI18N
        btnVenueEdit.setToolTipText("Edit item");
        btnVenueEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnVenueEditMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnVenueEditMouseEntered(evt);
            }
        });
        btnVenueEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVenueEditActionPerformed(evt);
            }
        });

        btnVenueDelete.setBackground(new java.awt.Color(35, 62, 104));
        btnVenueDelete.setForeground(new java.awt.Color(255, 255, 255));
        btnVenueDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_clear_white_18dp_2x.png"))); // NOI18N
        btnVenueDelete.setToolTipText("Delete Item");
        btnVenueDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnVenueDeleteMouseEntered(evt);
            }
        });
        btnVenueDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVenueDeleteActionPerformed(evt);
            }
        });

        txtVenueSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtVenueSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtVenueSearchMouseEntered(evt);
            }
        });

        btnVenueSearch.setBackground(new java.awt.Color(35, 62, 104));
        btnVenueSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnVenueSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_search_white_24dp_1x.png"))); // NOI18N
        btnVenueSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnVenueSearchMouseEntered(evt);
            }
        });
        btnVenueSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVenueSearchActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        txtVenueName.setEditable(false);
        txtVenueName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtVenueNameMouseEntered(evt);
            }
        });

        txtPostcode.setEditable(false);
        txtPostcode.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtPostcodeMouseEntered(evt);
            }
        });

        txtMaxCapacity.setEditable(false);
        txtMaxCapacity.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtMaxCapacityMouseEntered(evt);
            }
        });

        txtStandingSpaces.setEditable(false);
        txtStandingSpaces.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtStandingSpacesMouseEntered(evt);
            }
        });
        txtStandingSpaces.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtStandingSpacesKeyReleased(evt);
            }
        });

        btnVenueDone.setBackground(new java.awt.Color(35, 62, 104));
        btnVenueDone.setForeground(new java.awt.Color(255, 255, 255));
        btnVenueDone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_done_white_18dp_2x.png"))); // NOI18N
        btnVenueDone.setToolTipText("Add new Item");
        btnVenueDone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnVenueDoneMouseEntered(evt);
            }
        });
        btnVenueDone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVenueDoneActionPerformed(evt);
            }
        });

        btnVenueSave.setBackground(new java.awt.Color(35, 62, 104));
        btnVenueSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_save_white_18dp_2x.png"))); // NOI18N
        btnVenueSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnVenueSaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnVenueSaveMouseEntered(evt);
            }
        });
        btnVenueSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVenueSaveActionPerformed(evt);
            }
        });

        btnVenueCancel.setBackground(new java.awt.Color(35, 62, 104));
        btnVenueCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_block_white_18dp_2x.png"))); // NOI18N
        btnVenueCancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnVenueCancelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnVenueCancelMouseEntered(evt);
            }
        });
        btnVenueCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVenueCancelActionPerformed(evt);
            }
        });

        lblStandingSpaces.setForeground(new java.awt.Color(45, 80, 134));
        lblStandingSpaces.setText("Standing Spaces:");

        lblMaxCapacity.setForeground(new java.awt.Color(45, 80, 134));
        lblMaxCapacity.setText("Maximum Capacity:");

        lblPostCode.setForeground(new java.awt.Color(45, 80, 134));
        lblPostCode.setText("Postcode:");

        lblVenueName.setForeground(new java.awt.Color(45, 80, 134));
        lblVenueName.setText("Venue Name:");

        txtSeatedSpaces.setEditable(false);
        txtSeatedSpaces.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtSeatedSpacesMouseEntered(evt);
            }
        });
        txtSeatedSpaces.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSeatedSpacesKeyReleased(evt);
            }
        });

        txtAddressOne.setEditable(false);
        txtAddressOne.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtAddressOneMouseEntered(evt);
            }
        });

        lblAddressOne.setForeground(new java.awt.Color(45, 80, 134));
        lblAddressOne.setText("Address:");

        lblSeatingVenue.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lblSeatingVenue.setForeground(new java.awt.Color(45, 80, 134));
        lblSeatingVenue.setText("Seating");

        lblSeatedSpaces.setForeground(new java.awt.Color(45, 80, 134));
        lblSeatedSpaces.setText("Seated Spaces:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblVenueName)
                            .addComponent(lblAddressOne)
                            .addComponent(lblPostCode))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtAddressOne, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtVenueName, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtPostcode, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnVenueDone)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnVenueSave)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnVenueCancel))
                            .addComponent(lblSeatingVenue)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblMaxCapacity)
                                    .addComponent(lblStandingSpaces)
                                    .addComponent(lblSeatedSpaces))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtSeatedSpaces, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(txtStandingSpaces, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtMaxCapacity, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtVenueName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblVenueName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddressOne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAddressOne))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPostcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPostCode))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSeatingVenue)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtStandingSpaces, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblStandingSpaces))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSeatedSpaces, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSeatedSpaces))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMaxCapacity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMaxCapacity))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnVenueDone, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnVenueSave, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnVenueCancel, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        btnVenueDone.setContentAreaFilled(false);
        btnVenueDone.setOpaque(true);
        btnVenueSave.setContentAreaFilled(false);
        btnVenueSave.setOpaque(true);
        btnVenueCancel.setContentAreaFilled(false);
        btnVenueCancel.setOpaque(true);

        btnSearchVenueClear.setBackground(new java.awt.Color(45, 80, 134));
        btnSearchVenueClear.setForeground(new java.awt.Color(255, 255, 255));
        btnSearchVenueClear.setText("x");
        btnSearchVenueClear.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSearchVenueClear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSearchVenueClearMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSearchVenueClearMouseExited(evt);
            }
        });
        btnSearchVenueClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchVenueClearActionPerformed(evt);
            }
        });

        btnVenueHome.setBackground(new java.awt.Color(35, 62, 104));
        btnVenueHome.setForeground(new java.awt.Color(255, 255, 255));
        btnVenueHome.setText("Back to Homescreen");
        btnVenueHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnVenueHomeMouseEntered(evt);
            }
        });
        btnVenueHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVenueHomeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelVenueLayout = new javax.swing.GroupLayout(jPanelVenue);
        jPanelVenue.setLayout(jPanelVenueLayout);
        jPanelVenueLayout.setHorizontalGroup(
            jPanelVenueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelVenueLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelVenueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelVenueLayout.createSequentialGroup()
                        .addComponent(btnVenueSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtVenueSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearchVenueClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanelVenueLayout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanelVenueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelVenueLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelVenueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnVenueDelete)
                                    .addComponent(btnVenueEdit)
                                    .addComponent(btnVenueAdd))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(62, 234, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelVenueLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnVenueHome)
                                .addContainerGap())))))
        );
        jPanelVenueLayout.setVerticalGroup(
            jPanelVenueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelVenueLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(jPanelVenueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnVenueSearch, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelVenueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtVenueSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSearchVenueClear, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelVenueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanelVenueLayout.createSequentialGroup()
                        .addGroup(jPanelVenueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelVenueLayout.createSequentialGroup()
                                .addComponent(btnVenueAdd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnVenueEdit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnVenueDelete))
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 233, Short.MAX_VALUE)
                        .addComponent(btnVenueHome)))
                .addContainerGap())
        );

        btnVenueAdd.setContentAreaFilled(false);
        btnVenueAdd.setOpaque(true);
        btnVenueEdit.setContentAreaFilled(false);
        btnVenueEdit.setOpaque(true);
        btnVenueDelete.setContentAreaFilled(false);
        btnVenueDelete.setOpaque(true);
        btnVenueSearch.setContentAreaFilled(false);
        btnVenueSearch.setOpaque(true);
        btnSearchVenueClear.setContentAreaFilled(false);
        btnSearchVenueClear.setOpaque(true);
        btnVenueHome.setContentAreaFilled(false);
        btnVenueHome.setOpaque(true);

        tabPane.addTab("", new javax.swing.ImageIcon(getClass().getResource("/Icons/VenuesIcon.png")), jPanelVenue); // NOI18N

        jPanelTickets.setBackground(new java.awt.Color(255, 255, 255));
        jPanelTickets.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanelTicketsMouseEntered(evt);
            }
        });

        lstTicketList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "ACDC Goes Live", "Justin Bieber World Tour" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        lstTicketList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstTicketListMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lstTicketListMouseEntered(evt);
            }
        });
        jScrollPane4.setViewportView(lstTicketList);

        btnTicketAdd.setBackground(new java.awt.Color(35, 62, 104));
        btnTicketAdd.setForeground(new java.awt.Color(255, 255, 255));
        btnTicketAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_add_white_18dp_2x.png"))); // NOI18N
        btnTicketAdd.setToolTipText("Add new Item");
        btnTicketAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnTicketAddMouseEntered(evt);
            }
        });
        btnTicketAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTicketAddActionPerformed(evt);
            }
        });

        btnTicketEdit.setBackground(new java.awt.Color(35, 62, 104));
        btnTicketEdit.setForeground(new java.awt.Color(255, 255, 255));
        btnTicketEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_create_white_18dp_2x.png"))); // NOI18N
        btnTicketEdit.setToolTipText("Edit item");
        btnTicketEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnTicketEditMouseEntered(evt);
            }
        });
        btnTicketEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTicketEditActionPerformed(evt);
            }
        });

        btnTicketDelete.setBackground(new java.awt.Color(35, 62, 104));
        btnTicketDelete.setForeground(new java.awt.Color(255, 255, 255));
        btnTicketDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_clear_white_18dp_2x.png"))); // NOI18N
        btnTicketDelete.setToolTipText("Delete Item");
        btnTicketDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnTicketDeleteMouseEntered(evt);
            }
        });
        btnTicketDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTicketDeleteActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        btnTicketDone.setBackground(new java.awt.Color(35, 62, 104));
        btnTicketDone.setForeground(new java.awt.Color(255, 255, 255));
        btnTicketDone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_done_white_18dp_2x.png"))); // NOI18N
        btnTicketDone.setToolTipText("Add new Item");
        btnTicketDone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnTicketDoneMouseEntered(evt);
            }
        });
        btnTicketDone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTicketDoneActionPerformed(evt);
            }
        });

        btnTicketCancel.setBackground(new java.awt.Color(35, 62, 104));
        btnTicketCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_block_white_18dp_2x.png"))); // NOI18N
        btnTicketCancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnTicketCancelMouseEntered(evt);
            }
        });
        btnTicketCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTicketCancelActionPerformed(evt);
            }
        });

        btnTicketSave.setBackground(new java.awt.Color(35, 62, 104));
        btnTicketSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_save_white_18dp_2x.png"))); // NOI18N
        btnTicketSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnTicketSaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnTicketSaveMouseEntered(evt);
            }
        });
        btnTicketSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTicketSaveActionPerformed(evt);
            }
        });

        lblTicketUser.setForeground(new java.awt.Color(45, 80, 134));
        lblTicketUser.setText("User:");

        lblTicketEvent.setForeground(new java.awt.Color(45, 80, 134));
        lblTicketEvent.setText("Event:");
        lblTicketEvent.setToolTipText("");

        txtTicketUser.setEditable(false);
        txtTicketUser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtTicketUserMouseEntered(evt);
            }
        });

        txtTicketEvent.setEditable(false);
        txtTicketEvent.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtTicketEventMouseEntered(evt);
            }
        });

        txtTicketType.setEditable(false);
        txtTicketType.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtTicketTypeMouseEntered(evt);
            }
        });

        cmbTicketUser.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        cmbTicketEvent.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        lblTicketType.setForeground(new java.awt.Color(45, 80, 134));
        lblTicketType.setText("Ticket Type:");

        cmbTicketType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SEATED", "STANDING" }));
        cmbTicketType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTicketTypeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(btnTicketDone)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnTicketSave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnTicketCancel))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTicketEvent)
                            .addComponent(lblTicketUser)
                            .addComponent(lblTicketType))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtTicketType, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbTicketType, 0, 142, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtTicketUser, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                                    .addComponent(txtTicketEvent))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cmbTicketEvent, 0, 142, Short.MAX_VALUE)
                                    .addComponent(cmbTicketUser, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTicketUser)
                    .addComponent(txtTicketUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbTicketUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTicketEvent)
                    .addComponent(txtTicketEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbTicketEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTicketType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTicketType)
                    .addComponent(cmbTicketType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnTicketDone)
                    .addComponent(btnTicketSave)
                    .addComponent(btnTicketCancel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnTicketDone.setContentAreaFilled(false);
        btnTicketDone.setOpaque(true);
        btnTicketCancel.setContentAreaFilled(false);
        btnTicketCancel.setOpaque(true);
        btnTicketSave.setContentAreaFilled(false);
        btnTicketSave.setOpaque(true);

        btnTicketHome.setBackground(new java.awt.Color(35, 62, 104));
        btnTicketHome.setForeground(new java.awt.Color(255, 255, 255));
        btnTicketHome.setText("Back to Homescreen");
        btnTicketHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnTicketHomeMouseEntered(evt);
            }
        });
        btnTicketHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTicketHomeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelTicketsLayout = new javax.swing.GroupLayout(jPanelTickets);
        jPanelTickets.setLayout(jPanelTicketsLayout);
        jPanelTicketsLayout.setHorizontalGroup(
            jPanelTicketsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTicketsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanelTicketsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelTicketsLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelTicketsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnTicketDelete)
                            .addComponent(btnTicketEdit)
                            .addComponent(btnTicketAdd))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(164, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelTicketsLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnTicketHome)
                        .addContainerGap())))
        );
        jPanelTicketsLayout.setVerticalGroup(
            jPanelTicketsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTicketsLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(jPanelTicketsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addGroup(jPanelTicketsLayout.createSequentialGroup()
                        .addGroup(jPanelTicketsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelTicketsLayout.createSequentialGroup()
                                .addComponent(btnTicketAdd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTicketEdit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTicketDelete))
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 336, Short.MAX_VALUE)
                        .addComponent(btnTicketHome)))
                .addContainerGap())
        );

        btnTicketAdd.setContentAreaFilled(false);
        btnTicketAdd.setOpaque(true);
        btnTicketEdit.setContentAreaFilled(false);
        btnTicketEdit.setOpaque(true);
        btnTicketDelete.setContentAreaFilled(false);
        btnTicketDelete.setOpaque(true);
        btnTicketHome.setContentAreaFilled(false);
        btnTicketHome.setOpaque(true);

        tabPane.addTab("", new javax.swing.ImageIcon(getClass().getResource("/Icons/TicketIcon.png")), jPanelTickets); // NOI18N

        jPanelCompanies.setBackground(new java.awt.Color(255, 255, 255));
        jPanelCompanies.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanelCompaniesMouseEntered(evt);
            }
        });

        btnCompanySearch.setBackground(new java.awt.Color(35, 62, 104));
        btnCompanySearch.setForeground(new java.awt.Color(255, 255, 255));
        btnCompanySearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_search_white_24dp_1x.png"))); // NOI18N
        btnCompanySearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCompanySearchMouseEntered(evt);
            }
        });
        btnCompanySearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompanySearchActionPerformed(evt);
            }
        });

        txtCompanySearch.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtCompanySearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtCompanySearchMouseEntered(evt);
            }
        });

        btnSearchCompanyClear.setBackground(new java.awt.Color(45, 80, 134));
        btnSearchCompanyClear.setForeground(new java.awt.Color(255, 255, 255));
        btnSearchCompanyClear.setText("x");
        btnSearchCompanyClear.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSearchCompanyClear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSearchCompanyClearMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSearchCompanyClearMouseExited(evt);
            }
        });
        btnSearchCompanyClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchCompanyClearActionPerformed(evt);
            }
        });

        lstCompanyList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "ACDC Goes Live", "Justin Bieber World Tour" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        lstCompanyList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstCompanyListMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lstCompanyListMouseEntered(evt);
            }
        });
        jScrollPane9.setViewportView(lstCompanyList);

        btnCompanyDelete.setBackground(new java.awt.Color(35, 62, 104));
        btnCompanyDelete.setForeground(new java.awt.Color(255, 255, 255));
        btnCompanyDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_clear_white_18dp_2x.png"))); // NOI18N
        btnCompanyDelete.setToolTipText("Delete Item");
        btnCompanyDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCompanyDeleteMouseEntered(evt);
            }
        });
        btnCompanyDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompanyDeleteActionPerformed(evt);
            }
        });

        btnCompanyEdit.setBackground(new java.awt.Color(35, 62, 104));
        btnCompanyEdit.setForeground(new java.awt.Color(255, 255, 255));
        btnCompanyEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_create_white_18dp_2x.png"))); // NOI18N
        btnCompanyEdit.setToolTipText("Edit item");
        btnCompanyEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCompanyEditMouseEntered(evt);
            }
        });
        btnCompanyEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompanyEditActionPerformed(evt);
            }
        });

        btnCompanyAdd.setBackground(new java.awt.Color(35, 62, 104));
        btnCompanyAdd.setForeground(new java.awt.Color(255, 255, 255));
        btnCompanyAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_add_white_18dp_2x.png"))); // NOI18N
        btnCompanyAdd.setToolTipText("Add new Item");
        btnCompanyAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCompanyAddMouseEntered(evt);
            }
        });
        btnCompanyAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompanyAddActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        lblCompanyName.setForeground(new java.awt.Color(45, 80, 134));
        lblCompanyName.setText("Company Name:");

        txtCompanyName.setEditable(false);
        txtCompanyName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtCompanyNameMouseEntered(evt);
            }
        });

        txtCompanyEmailAddress.setEditable(false);
        txtCompanyEmailAddress.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtCompanyEmailAddressMouseEntered(evt);
            }
        });

        txtCompanyPhoneNumber.setEditable(false);
        txtCompanyPhoneNumber.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtCompanyPhoneNumberMouseEntered(evt);
            }
        });

        lblEmailAddress.setForeground(new java.awt.Color(45, 80, 134));
        lblEmailAddress.setText("Email Address:");

        lblPhoneNumber.setForeground(new java.awt.Color(45, 80, 134));
        lblPhoneNumber.setText("Phone Number:");

        btnCompanyDone.setBackground(new java.awt.Color(35, 62, 104));
        btnCompanyDone.setForeground(new java.awt.Color(255, 255, 255));
        btnCompanyDone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_done_white_18dp_2x.png"))); // NOI18N
        btnCompanyDone.setToolTipText("Add new Item");
        btnCompanyDone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCompanyDoneMouseEntered(evt);
            }
        });
        btnCompanyDone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompanyDoneActionPerformed(evt);
            }
        });

        btnCompanySave.setBackground(new java.awt.Color(35, 62, 104));
        btnCompanySave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_save_white_18dp_2x.png"))); // NOI18N
        btnCompanySave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCompanySaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCompanySaveMouseEntered(evt);
            }
        });
        btnCompanySave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompanySaveActionPerformed(evt);
            }
        });

        btnCompanyCancel.setBackground(new java.awt.Color(35, 62, 104));
        btnCompanyCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_block_white_18dp_2x.png"))); // NOI18N
        btnCompanyCancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCompanyCancelMouseEntered(evt);
            }
        });
        btnCompanyCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompanyCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCompanyName)
                            .addComponent(lblEmailAddress)
                            .addComponent(lblPhoneNumber))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtCompanyEmailAddress, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                            .addComponent(txtCompanyName, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCompanyPhoneNumber)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(btnCompanyDone)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCompanySave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCompanyCancel)))
                .addContainerGap(64, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCompanyName)
                    .addComponent(txtCompanyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCompanyEmailAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEmailAddress))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCompanyPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPhoneNumber))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCompanySave)
                    .addComponent(btnCompanyDone)
                    .addComponent(btnCompanyCancel))
                .addContainerGap())
        );

        btnCompanyDone.setContentAreaFilled(false);
        btnCompanyDone.setOpaque(true);
        btnCompanySave.setContentAreaFilled(false);
        btnCompanySave.setOpaque(true);
        btnCompanyCancel.setContentAreaFilled(false);
        btnCompanyCancel.setOpaque(true);

        btnCompaniesHome.setBackground(new java.awt.Color(35, 62, 104));
        btnCompaniesHome.setForeground(new java.awt.Color(255, 255, 255));
        btnCompaniesHome.setText("Back to Homescreen");
        btnCompaniesHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCompaniesHomeMouseEntered(evt);
            }
        });
        btnCompaniesHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompaniesHomeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelCompaniesLayout = new javax.swing.GroupLayout(jPanelCompanies);
        jPanelCompanies.setLayout(jPanelCompaniesLayout);
        jPanelCompaniesLayout.setHorizontalGroup(
            jPanelCompaniesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCompaniesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCompaniesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCompaniesLayout.createSequentialGroup()
                        .addComponent(btnCompanySearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCompanySearch, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearchCompanyClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(603, Short.MAX_VALUE))
                    .addGroup(jPanelCompaniesLayout.createSequentialGroup()
                        .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanelCompaniesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelCompaniesLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelCompaniesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnCompanyDelete)
                                    .addComponent(btnCompanyEdit)
                                    .addComponent(btnCompanyAdd))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 165, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCompaniesLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnCompaniesHome)
                                .addContainerGap())))))
        );
        jPanelCompaniesLayout.setVerticalGroup(
            jPanelCompaniesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCompaniesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCompaniesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCompaniesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtCompanySearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnCompanySearch, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addComponent(btnSearchCompanyClear, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelCompaniesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9)
                    .addGroup(jPanelCompaniesLayout.createSequentialGroup()
                        .addGroup(jPanelCompaniesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelCompaniesLayout.createSequentialGroup()
                                .addComponent(btnCompanyAdd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCompanyEdit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCompanyDelete))
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 332, Short.MAX_VALUE)
                        .addComponent(btnCompaniesHome)))
                .addContainerGap())
        );

        btnCompanySearch.setContentAreaFilled(false);
        btnCompanySearch.setOpaque(true);
        btnSearchCompanyClear.setContentAreaFilled(false);
        btnSearchCompanyClear.setOpaque(true);
        btnCompanyDelete.setContentAreaFilled(false);
        btnCompanyDelete.setOpaque(true);
        btnCompanyEdit.setContentAreaFilled(false);
        btnCompanyEdit.setOpaque(true);
        btnCompanyAdd.setContentAreaFilled(false);
        btnCompanyAdd.setOpaque(true);
        btnCompaniesHome.setContentAreaFilled(false);
        btnCompaniesHome.setOpaque(true);

        tabPane.addTab("", new javax.swing.ImageIcon(getClass().getResource("/Icons/CompaniesIcon.png")), jPanelCompanies); // NOI18N

        jPanelUsers.setBackground(new java.awt.Color(255, 255, 255));
        jPanelUsers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanelUsersMouseEntered(evt);
            }
        });

        btnUserSearch.setBackground(new java.awt.Color(35, 62, 104));
        btnUserSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnUserSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_search_white_24dp_1x.png"))); // NOI18N
        btnUserSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUserSearchMouseEntered(evt);
            }
        });
        btnUserSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserSearchActionPerformed(evt);
            }
        });

        txtUserSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtUserSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtUserSearchMouseEntered(evt);
            }
        });

        btnSearchUserClear.setBackground(new java.awt.Color(45, 80, 134));
        btnSearchUserClear.setForeground(new java.awt.Color(255, 255, 255));
        btnSearchUserClear.setText("x");
        btnSearchUserClear.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSearchUserClear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSearchUserClearMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSearchUserClearMouseExited(evt);
            }
        });
        btnSearchUserClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchUserClearActionPerformed(evt);
            }
        });

        lstUserList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "ACDC Goes Live", "Justin Bieber World Tour" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        lstUserList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstUserListMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lstUserListMouseEntered(evt);
            }
        });
        jScrollPane10.setViewportView(lstUserList);

        btnUserDelete.setBackground(new java.awt.Color(35, 62, 104));
        btnUserDelete.setForeground(new java.awt.Color(255, 255, 255));
        btnUserDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_clear_white_18dp_2x.png"))); // NOI18N
        btnUserDelete.setToolTipText("Delete Item");
        btnUserDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUserDeleteMouseEntered(evt);
            }
        });
        btnUserDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserDeleteActionPerformed(evt);
            }
        });

        btnUserEdit.setBackground(new java.awt.Color(35, 62, 104));
        btnUserEdit.setForeground(new java.awt.Color(255, 255, 255));
        btnUserEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_create_white_18dp_2x.png"))); // NOI18N
        btnUserEdit.setToolTipText("Edit item");
        btnUserEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUserEditMouseEntered(evt);
            }
        });
        btnUserEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserEditActionPerformed(evt);
            }
        });

        btnUserAdd.setBackground(new java.awt.Color(35, 62, 104));
        btnUserAdd.setForeground(new java.awt.Color(255, 255, 255));
        btnUserAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_add_white_18dp_2x.png"))); // NOI18N
        btnUserAdd.setToolTipText("Add new Item");
        btnUserAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUserAddMouseEntered(evt);
            }
        });
        btnUserAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserAddActionPerformed(evt);
            }
        });

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        lblUserFirstName.setForeground(new java.awt.Color(45, 80, 134));
        lblUserFirstName.setText("First name:");

        txtUserFirstName.setEditable(false);
        txtUserFirstName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtUserFirstNameMouseEntered(evt);
            }
        });

        lblUserLastName.setForeground(new java.awt.Color(45, 80, 134));
        lblUserLastName.setText("Last name:");

        txtUserLastName.setEditable(false);
        txtUserLastName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtUserLastNameMouseEntered(evt);
            }
        });

        lblUserDOB.setForeground(new java.awt.Color(45, 80, 134));
        lblUserDOB.setText("Date of Birth:");

        lblUserTelephoneNumber.setForeground(new java.awt.Color(45, 80, 134));
        lblUserTelephoneNumber.setText("Telephone Number:");

        lblUserEmailAddress.setForeground(new java.awt.Color(45, 80, 134));
        lblUserEmailAddress.setText("Email Address");

        lblUserType.setForeground(new java.awt.Color(45, 80, 134));
        lblUserType.setText("User Type:");

        lblUserPassword.setForeground(new java.awt.Color(45, 80, 134));
        lblUserPassword.setText("Password:");

        lblUserAddressTitle.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lblUserAddressTitle.setForeground(new java.awt.Color(45, 80, 134));
        lblUserAddressTitle.setText("Address");

        lblUserAddress.setForeground(new java.awt.Color(45, 80, 134));
        lblUserAddress.setText("Address:");

        lblUserPostCode.setForeground(new java.awt.Color(45, 80, 134));
        lblUserPostCode.setText("Postcode:");

        txtUserTelephoneNumber.setEditable(false);
        txtUserTelephoneNumber.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtUserTelephoneNumberMouseEntered(evt);
            }
        });

        txtUserDOB.setEditable(false);
        txtUserDOB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtUserDOBMouseEntered(evt);
            }
        });

        txtUserEmailAddress.setEditable(false);
        txtUserEmailAddress.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtUserEmailAddressMouseEntered(evt);
            }
        });

        txtUserPassword.setEditable(false);
        txtUserPassword.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtUserPasswordMouseEntered(evt);
            }
        });

        txtUserAddress.setEditable(false);
        txtUserAddress.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtUserAddressMouseEntered(evt);
            }
        });

        txtUserPostCode.setEditable(false);
        txtUserPostCode.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtUserPostCodeMouseEntered(evt);
            }
        });

        btnUserDone.setBackground(new java.awt.Color(35, 62, 104));
        btnUserDone.setForeground(new java.awt.Color(255, 255, 255));
        btnUserDone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_done_white_18dp_2x.png"))); // NOI18N
        btnUserDone.setToolTipText("Add new Item");
        btnUserDone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUserDoneMouseEntered(evt);
            }
        });
        btnUserDone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserDoneActionPerformed(evt);
            }
        });

        btnUserSave.setBackground(new java.awt.Color(35, 62, 104));
        btnUserSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_save_white_18dp_2x.png"))); // NOI18N
        btnUserSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnUserSaveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUserSaveMouseEntered(evt);
            }
        });
        btnUserSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserSaveActionPerformed(evt);
            }
        });

        btnUserCancel.setBackground(new java.awt.Color(35, 62, 104));
        btnUserCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/ic_block_white_18dp_2x.png"))); // NOI18N
        btnUserCancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUserCancelMouseEntered(evt);
            }
        });
        btnUserCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserCancelActionPerformed(evt);
            }
        });

        btnUserBookTicket.setBackground(new java.awt.Color(35, 62, 104));
        btnUserBookTicket.setForeground(new java.awt.Color(255, 255, 255));
        btnUserBookTicket.setText("Book a ticket for this user");
        btnUserBookTicket.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUserBookTicketMouseEntered(evt);
            }
        });
        btnUserBookTicket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserBookTicketActionPerformed(evt);
            }
        });

        cmbUserType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ADMIN", "USER", "GUEST" }));
        cmbUserType.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                cmbUserTypePopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        cmbUserType.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cmbUserTypeMouseEntered(evt);
            }
        });

        txtUserType.setEditable(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txtUserDOB, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblUserFirstName)
                                .addComponent(lblUserLastName))
                            .addGap(44, 44, 44)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtUserFirstName, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
                                .addComponent(txtUserLastName))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnUserDone)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUserSave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUserCancel))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblUserTelephoneNumber)
                                .addComponent(lblUserDOB)
                                .addComponent(lblUserEmailAddress))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtUserEmailAddress, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
                                .addComponent(txtUserTelephoneNumber)))
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(lblUserType)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                            .addComponent(txtUserType, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(lblUserAddress)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtUserAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblUserPostCode)
                                .addComponent(lblUserPassword))
                            .addGap(4, 4, 4)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtUserPostCode, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnUserBookTicket, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel4Layout.createSequentialGroup()
                                    .addGap(44, 44, 44)
                                    .addComponent(txtUserPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addComponent(cmbUserType, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblUserAddressTitle))
                .addGap(0, 210, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserFirstName)
                    .addComponent(txtUserFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserLastName)
                    .addComponent(txtUserLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserDOB)
                    .addComponent(txtUserDOB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserTelephoneNumber)
                    .addComponent(txtUserTelephoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserEmailAddress)
                    .addComponent(txtUserEmailAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserType)
                    .addComponent(txtUserType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbUserType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUserPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblUserPassword))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblUserAddressTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserAddress)
                    .addComponent(txtUserAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserPostCode)
                    .addComponent(txtUserPostCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnUserBookTicket, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnUserCancel, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnUserSave, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnUserDone, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        btnUserDone.setContentAreaFilled(false);
        btnUserDone.setOpaque(true);
        btnUserSave.setContentAreaFilled(false);
        btnUserSave.setOpaque(true);
        btnUserCancel.setContentAreaFilled(false);
        btnUserCancel.setOpaque(true);
        btnUserBookTicket.setContentAreaFilled(false);
        btnUserBookTicket.setOpaque(true);

        btnUsersHome.setBackground(new java.awt.Color(35, 62, 104));
        btnUsersHome.setForeground(new java.awt.Color(255, 255, 255));
        btnUsersHome.setText("Back to Homescreen");
        btnUsersHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUsersHomeMouseEntered(evt);
            }
        });
        btnUsersHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUsersHomeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelUsersLayout = new javax.swing.GroupLayout(jPanelUsers);
        jPanelUsers.setLayout(jPanelUsersLayout);
        jPanelUsersLayout.setHorizontalGroup(
            jPanelUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUsersLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelUsersLayout.createSequentialGroup()
                        .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanelUsersLayout.createSequentialGroup()
                                .addGroup(jPanelUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnUserDelete)
                                    .addComponent(btnUserEdit)
                                    .addComponent(btnUserAdd))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnUsersHome)))
                    .addGroup(jPanelUsersLayout.createSequentialGroup()
                        .addComponent(btnUserSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtUserSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearchUserClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelUsersLayout.setVerticalGroup(
            jPanelUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUsersLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtUserSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnUserSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addComponent(btnSearchUserClear, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane10)
                    .addGroup(jPanelUsersLayout.createSequentialGroup()
                        .addGroup(jPanelUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelUsersLayout.createSequentialGroup()
                                .addComponent(btnUserAdd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnUserEdit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnUserDelete))
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 83, Short.MAX_VALUE)
                        .addComponent(btnUsersHome)))
                .addContainerGap())
        );

        btnUserSearch.setContentAreaFilled(false);
        btnUserSearch.setOpaque(true);
        btnSearchUserClear.setContentAreaFilled(false);
        btnSearchUserClear.setOpaque(true);
        btnUserDelete.setContentAreaFilled(false);
        btnUserDelete.setOpaque(true);
        btnUserEdit.setContentAreaFilled(false);
        btnUserEdit.setOpaque(true);
        btnUserAdd.setContentAreaFilled(false);
        btnUserAdd.setOpaque(true);
        btnUsersHome.setContentAreaFilled(false);
        btnUsersHome.setOpaque(true);

        tabPane.addTab("", new javax.swing.ImageIcon(getClass().getResource("/Icons/UsersIcon.png")), jPanelUsers); // NOI18N

        eventSystemLabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        eventSystemLabel.setForeground(new java.awt.Color(255, 255, 255));
        eventSystemLabel.setText("Event System Application");

        lblLiveHelp.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblLiveHelp.setForeground(new java.awt.Color(255, 255, 255));
        lblLiveHelp.setText("Help");

        txtLiveHelp.setEditable(false);
        txtLiveHelp.setColumns(20);
        txtLiveHelp.setLineWrap(true);
        txtLiveHelp.setRows(5);
        txtLiveHelp.setWrapStyleWord(true);
        jScrollPane2.setViewportView(txtLiveHelp);

        btnExit.setBackground(new java.awt.Color(45, 80, 134));
        btnExit.setForeground(new java.awt.Color(255, 255, 255));
        btnExit.setText("x");
        btnExit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnExitMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnExitMouseExited(evt);
            }
        });
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        lblLogHistory.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblLogHistory.setForeground(new java.awt.Color(255, 255, 255));
        lblLogHistory.setText("Log History");

        txtLogHistory.setEditable(false);
        txtLogHistory.setColumns(20);
        txtLogHistory.setLineWrap(true);
        txtLogHistory.setRows(5);
        txtLogHistory.setWrapStyleWord(true);
        jScrollPane5.setViewportView(txtLogHistory);

        btnLogHistoryClear.setBackground(new java.awt.Color(255, 255, 255));
        btnLogHistoryClear.setForeground(new java.awt.Color(35, 62, 104));
        btnLogHistoryClear.setText("Clear");
        btnLogHistoryClear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnLogHistoryClearMouseClicked(evt);
            }
        });

        btnRefresh.setBackground(new java.awt.Color(255, 255, 255));
        btnRefresh.setForeground(new java.awt.Color(255, 255, 255));
        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/refreshIcon.png"))); // NOI18N
        btnRefresh.setToolTipText("Refresh All List");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(eventSystemLabel))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblLogHistory))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnLogHistoryClear)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnRefresh))
                            .addComponent(lblLiveHelp)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(900, 900, 900)
                        .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(tabPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 932, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(eventSystemLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblLogHistory)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnRefresh)
                            .addComponent(btnLogHistoryClear))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblLiveHelp)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(tabPane, javax.swing.GroupLayout.PREFERRED_SIZE, 642, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        btnEventAdd.setContentAreaFilled(false);
        btnEventAdd.setOpaque(true);
        btnExit.setContentAreaFilled(false);
        btnExit.setOpaque(true);
        btnLogHistoryClear.setContentAreaFilled(false);
        btnLogHistoryClear.setOpaque(true);
        btnRefresh.setContentAreaFilled(false);
        btnRefresh.setOpaque(true);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnExitMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnExitMouseEntered
        btnExit.setBackground(new Color(0xb31919));
    }//GEN-LAST:event_btnExitMouseEntered

    private void btnExitMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnExitMouseExited
        btnExit.setBackground(primary);
    }//GEN-LAST:event_btnExitMouseExited

    private void btnLogHistoryClearMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLogHistoryClearMouseClicked
        txtLogHistory.setText("");
    }//GEN-LAST:event_btnLogHistoryClearMouseClicked

    private void jPanelTicketsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelTicketsMouseEntered
        txtLiveHelp.setText("");
    }//GEN-LAST:event_jPanelTicketsMouseEntered

    private void btnTicketSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTicketSaveActionPerformed
        editTicket();
    }//GEN-LAST:event_btnTicketSaveActionPerformed

    private void btnTicketSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTicketSaveMouseEntered
        txtLiveHelp.setText("Clicking this button will confirm and make"
            + " changes to the system with the new values.");
    }//GEN-LAST:event_btnTicketSaveMouseEntered

    private void btnTicketSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTicketSaveMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnTicketSaveMouseClicked

    private void btnTicketCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTicketCancelActionPerformed
        cancelAddEditTicket();
    }//GEN-LAST:event_btnTicketCancelActionPerformed

    private void btnTicketCancelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTicketCancelMouseEntered
        txtLiveHelp.setText("Clicking this button will cancel the changes.");
    }//GEN-LAST:event_btnTicketCancelMouseEntered

    private void btnTicketDoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTicketDoneActionPerformed
        addTicket();
    }//GEN-LAST:event_btnTicketDoneActionPerformed

    private void btnTicketDoneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTicketDoneMouseEntered
        txtLiveHelp.setText("Clicking the button will confirm the changes and add"
            + "a new ticket.");
    }//GEN-LAST:event_btnTicketDoneMouseEntered

    private void btnTicketDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTicketDeleteActionPerformed
        deleteTicket();
    }//GEN-LAST:event_btnTicketDeleteActionPerformed

    private void btnTicketDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTicketDeleteMouseEntered
        txtLiveHelp.setText("Clicking this button will allow you to"
            + " delete the selected ticket.");
    }//GEN-LAST:event_btnTicketDeleteMouseEntered

    private void btnTicketEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTicketEditActionPerformed
        setEditTicketUI();
    }//GEN-LAST:event_btnTicketEditActionPerformed

    private void btnTicketEditMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTicketEditMouseEntered
        txtLiveHelp.setText("Clicking this button will allow you to"
            + " edit the selected ticket.");
    }//GEN-LAST:event_btnTicketEditMouseEntered

    private void btnTicketAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTicketAddActionPerformed
        setAddTicketUI();
        //lstEventList;

    }//GEN-LAST:event_btnTicketAddActionPerformed

    private void btnTicketAddMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTicketAddMouseEntered
        txtLiveHelp.setText("Clicking the button will allow you to add"
            + " more tickets.");
    }//GEN-LAST:event_btnTicketAddMouseEntered

    private void lstTicketListMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstTicketListMouseEntered
        txtLiveHelp.setText("This is where all of the tickets are displayed"
            + ". Click on anyone of them to display more information about"
            + " the ticket.");
    }//GEN-LAST:event_lstTicketListMouseEntered

    private void lstTicketListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstTicketListMouseClicked
        IListable obj = getValue(this.ticketList, lstTicketList);
        if(null != obj) {
            if(obj.getClass() == new Ticket().getClass()) {
                Ticket ticket = (Ticket)obj;
                this.setTicketTextFields(ticket);
            }
        }
    }//GEN-LAST:event_lstTicketListMouseClicked

    private void jPanelVenueMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelVenueMouseEntered
        txtLiveHelp.setText("");
    }//GEN-LAST:event_jPanelVenueMouseEntered

    private void btnSearchVenueClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchVenueClearActionPerformed
        txtVenueSearch.setText("");
        this.venueList.updateList();
        refreshList(lstVenueList, venueList);
        this.btnSearchVenueClear.setVisible(false);
        this.txtVenueSearch.setEditable(true);
    }//GEN-LAST:event_btnSearchVenueClearActionPerformed

    private void btnSearchVenueClearMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSearchVenueClearMouseExited
        btnSearchVenueClear.setBackground(primary);
    }//GEN-LAST:event_btnSearchVenueClearMouseExited

    private void btnSearchVenueClearMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSearchVenueClearMouseEntered
        btnSearchVenueClear.setBackground(new Color(0xb31919));
    }//GEN-LAST:event_btnSearchVenueClearMouseEntered

    private void txtSeatedSpacesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSeatedSpacesKeyReleased
        calculateMaxSpaces();
    }//GEN-LAST:event_txtSeatedSpacesKeyReleased

    private void btnVenueCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVenueCancelActionPerformed
        cancelAddEditVenue();
    }//GEN-LAST:event_btnVenueCancelActionPerformed

    private void btnVenueCancelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVenueCancelMouseEntered
        txtLiveHelp.setText("Clicking this button will cancel the changes.");
    }//GEN-LAST:event_btnVenueCancelMouseEntered

    private void btnVenueCancelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVenueCancelMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnVenueCancelMouseClicked

    private void btnVenueSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVenueSaveActionPerformed
        editVenue();
    }//GEN-LAST:event_btnVenueSaveActionPerformed

    private void btnVenueSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVenueSaveMouseEntered
        txtLiveHelp.setText("Clicking this button will confirm and make"
            + " changes to the system with the new values.");
    }//GEN-LAST:event_btnVenueSaveMouseEntered

    private void btnVenueSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVenueSaveMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnVenueSaveMouseClicked

    private void btnVenueDoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVenueDoneActionPerformed
        addVenue();
    }//GEN-LAST:event_btnVenueDoneActionPerformed

    private void btnVenueDoneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVenueDoneMouseEntered
        txtLiveHelp.setText("Clicking the button will confirm the changes and add"
            + " a new venue.");
    }//GEN-LAST:event_btnVenueDoneMouseEntered

    private void txtStandingSpacesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStandingSpacesKeyReleased
        calculateMaxSpaces();
    }//GEN-LAST:event_txtStandingSpacesKeyReleased

    private void btnVenueSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVenueSearchActionPerformed
        searchVenues();
    }//GEN-LAST:event_btnVenueSearchActionPerformed

    private void btnVenueSearchMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVenueSearchMouseEntered
        txtLiveHelp.setText("Click this button to search for a venue.");
    }//GEN-LAST:event_btnVenueSearchMouseEntered

    private void txtVenueSearchMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtVenueSearchMouseEntered
        txtLiveHelp.setText("Here you can enter a venue to search for. "
            + "Once you have entered one click the search button.");
    }//GEN-LAST:event_txtVenueSearchMouseEntered

    private void btnVenueDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVenueDeleteActionPerformed
        deleteVenue();
    }//GEN-LAST:event_btnVenueDeleteActionPerformed

    private void btnVenueDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVenueDeleteMouseEntered
        txtLiveHelp.setText("Clicking this button will allow you to"
            + " delete the selected venue.");
    }//GEN-LAST:event_btnVenueDeleteMouseEntered

    private void btnVenueEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVenueEditActionPerformed
        setEditVenueUI();
    }//GEN-LAST:event_btnVenueEditActionPerformed

    private void btnVenueEditMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVenueEditMouseEntered
        txtLiveHelp.setText("Clicking this button will allow you to"
            + " edit the selected venue.");
    }//GEN-LAST:event_btnVenueEditMouseEntered

    private void btnVenueEditMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVenueEditMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnVenueEditMouseClicked

    private void btnVenueAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVenueAddActionPerformed
        setAddVenueUI();
    }//GEN-LAST:event_btnVenueAddActionPerformed

    private void btnVenueAddMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVenueAddMouseEntered
        txtLiveHelp.setText("Clicking the button will allow you to add"
            + " more venues.");
    }//GEN-LAST:event_btnVenueAddMouseEntered

    private void btnVenueAddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVenueAddMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnVenueAddMouseClicked

    private void lstVenueListMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstVenueListMouseEntered
        txtLiveHelp.setText("This is where all of the venues are displayed"
            + ". Click on anyone of them to display more information about"
            + " the venue.");
    }//GEN-LAST:event_lstVenueListMouseEntered

    private void lstVenueListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstVenueListMouseClicked
        IListable obj = getValue(this.venueList, lstVenueList);
        if(null != obj) {
            if(obj.getClass() == new Venue().getClass()) {
                Venue venue = (Venue)obj;
                this.setVenueTextFields(venue);
            }
        }
    }//GEN-LAST:event_lstVenueListMouseClicked

    private void jPanelEventsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelEventsMouseEntered
        txtLiveHelp.setText("");
    }//GEN-LAST:event_jPanelEventsMouseEntered

    private void btnSearchEventClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchEventClearActionPerformed
        txtEventSearch.setText("");
        this.eventList.updateList();
        refreshList(lstEventList, eventList);
        this.btnSearchEventClear.setVisible(false);
        this.txtEventSearch.setEditable(true);
    }//GEN-LAST:event_btnSearchEventClearActionPerformed

    private void btnSearchEventClearMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSearchEventClearMouseExited
        btnSearchEventClear.setBackground(primary);
    }//GEN-LAST:event_btnSearchEventClearMouseExited

    private void btnSearchEventClearMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSearchEventClearMouseEntered
        btnSearchEventClear.setBackground(new Color(0xb31919));
    }//GEN-LAST:event_btnSearchEventClearMouseEntered

    private void btnEventDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEventDeleteActionPerformed
        deleteEvent();
    }//GEN-LAST:event_btnEventDeleteActionPerformed

    private void btnEventDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventDeleteMouseEntered
        txtLiveHelp.setText("Clicking this button will allow you to"
            + " delete the selected event.");
    }//GEN-LAST:event_btnEventDeleteMouseEntered

    private void btnEventEditMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventEditMouseEntered
        txtLiveHelp.setText("Clicking this button will allow you to"
            + " edit the selected event.");
    }//GEN-LAST:event_btnEventEditMouseEntered

    private void btnEventEditMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventEditMouseClicked

    }//GEN-LAST:event_btnEventEditMouseClicked

    private void btnEventAddMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventAddMouseEntered
        txtLiveHelp.setText("Clicking the button will allow you to add"
            + " more events.");
    }//GEN-LAST:event_btnEventAddMouseEntered

    private void btnEventAddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventAddMouseClicked
        
    }//GEN-LAST:event_btnEventAddMouseClicked

    private void txtEventEndDateMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventEndDateMouseEntered
        txtLiveHelp.setText("This is the event end date, enter the format as DD:MM:YY");
    }//GEN-LAST:event_txtEventEndDateMouseEntered

    private void txtEventStartDateMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventStartDateMouseEntered
        txtLiveHelp.setText("This is the event start date, enter the format as DD:MM:YY");
    }//GEN-LAST:event_txtEventStartDateMouseEntered

    private void txtEventEndTimeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventEndTimeMouseEntered
        txtLiveHelp.setText("This is the event end time, enter the format as HH:MM:SS.");
    }//GEN-LAST:event_txtEventEndTimeMouseEntered

    private void txtEventStartTimeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventStartTimeMouseEntered
        txtLiveHelp.setText("This is the event start time, enter the format as HH:MM:SS.");
    }//GEN-LAST:event_txtEventStartTimeMouseEntered

    private void btnEventDoneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventDoneMouseEntered
        txtLiveHelp.setText("Clicking the button will confirm the changes and add"
            + " a new event.");
    }//GEN-LAST:event_btnEventDoneMouseEntered

    private void btnEventDoneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventDoneMouseClicked

    }//GEN-LAST:event_btnEventDoneMouseClicked

    private void txtEventAgeRestrictionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventAgeRestrictionMouseEntered
        txtLiveHelp.setText("This is the events age restriction, enter a number to set one otherwise use 0.");
    }//GEN-LAST:event_txtEventAgeRestrictionMouseEntered

    private void txtEventStandingPriceMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventStandingPriceMouseEntered
        txtLiveHelp.setText("This is the events standing price. note: no currency symbol is required.");
    }//GEN-LAST:event_txtEventStandingPriceMouseEntered

    private void txtEventSeatingPriceMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventSeatingPriceMouseEntered
        txtLiveHelp.setText("This is the events seating price. note: no currency symbol is required.");
    }//GEN-LAST:event_txtEventSeatingPriceMouseEntered

    private void cmbCompanyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCompanyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCompanyActionPerformed

    private void btnEventEditCancelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventEditCancelMouseEntered
        txtLiveHelp.setText("Clicking this button will cancel the changes.");
    }//GEN-LAST:event_btnEventEditCancelMouseEntered

    private void btnEventEditCancelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventEditCancelMouseClicked
        
    }//GEN-LAST:event_btnEventEditCancelMouseClicked

    private void btnEventSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEventSaveActionPerformed
        editEvent();
        //setEventTextFields((Event)this.getValue(eventList, lstEventList));
    }//GEN-LAST:event_btnEventSaveActionPerformed

    private void btnEventSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventSaveMouseEntered
        txtLiveHelp.setText("Clicking this button will confirm and make"
            + " changes to the system with the new values.");
    }//GEN-LAST:event_btnEventSaveMouseEntered

    private void btnEventSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventSaveMouseClicked

    }//GEN-LAST:event_btnEventSaveMouseClicked

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        searchEvents();
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnSearchMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSearchMouseEntered
        txtLiveHelp.setText("Click this button to search for an event.");
    }//GEN-LAST:event_btnSearchMouseEntered

    private void txtEventSearchMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventSearchMouseEntered
        txtLiveHelp.setText("Here you can enter an event to search for. "
            + "Once you have entered one click the search button.");
    }//GEN-LAST:event_txtEventSearchMouseEntered

    private void lstEventListMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstEventListMouseEntered
        txtLiveHelp.setText("This is where all of the events are displayed"
            + ". Click on anyone of them to display more information about"
            + " the event.");
    }//GEN-LAST:event_lstEventListMouseEntered

    private void lstEventListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstEventListMouseClicked
        IListable obj = getValue(this.eventList, lstEventList);
        if(null != obj) {
            if(obj.getClass() == new Event().getClass()) {
                Event event = (Event)obj;
                this.setEventTextFields(event);
            }
        }
    }//GEN-LAST:event_lstEventListMouseClicked

    private void btnCompanySearchMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCompanySearchMouseEntered
        txtLiveHelp.setText("Click this button to search for a company.");
    }//GEN-LAST:event_btnCompanySearchMouseEntered

    private void btnCompanySearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompanySearchActionPerformed
        searchCompanies();
    }//GEN-LAST:event_btnCompanySearchActionPerformed

    private void txtCompanySearchMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtCompanySearchMouseEntered
        txtLiveHelp.setText("Here you can enter a company to search for. "
            + "Once you have entered one click the search button.");
    }//GEN-LAST:event_txtCompanySearchMouseEntered

    private void btnSearchCompanyClearMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSearchCompanyClearMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSearchCompanyClearMouseEntered

    private void btnSearchCompanyClearMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSearchCompanyClearMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSearchCompanyClearMouseExited

    private void btnSearchCompanyClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchCompanyClearActionPerformed
        txtCompanySearch.setText("");
        this.companyList.updateList();
        refreshList(lstCompanyList, companyList);
        this.btnSearchCompanyClear.setVisible(false);
        this.txtCompanySearch.setEditable(true);
    }//GEN-LAST:event_btnSearchCompanyClearActionPerformed

    private void lstCompanyListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstCompanyListMouseClicked
        IListable obj = getValue(this.companyList, lstCompanyList);
        if(null != obj) {
            if(obj.getClass() == new Company().getClass()) {
                Company company = (Company)obj;
                this.setCompanyTextFields(company);
            }
        }
    }//GEN-LAST:event_lstCompanyListMouseClicked

    private void lstCompanyListMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstCompanyListMouseEntered
        txtLiveHelp.setText("This is where all of the companies are displayed"
            + ". Click on anyone of them to display more information about"
            + " the company.");
    }//GEN-LAST:event_lstCompanyListMouseEntered

    private void btnCompanyDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCompanyDeleteMouseEntered
        txtLiveHelp.setText("Clicking this button will allow you to"
            + " delete the selected company.");
    }//GEN-LAST:event_btnCompanyDeleteMouseEntered

    private void btnCompanyDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompanyDeleteActionPerformed
        deleteCompany();
    }//GEN-LAST:event_btnCompanyDeleteActionPerformed

    private void btnCompanyEditMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCompanyEditMouseEntered
        txtLiveHelp.setText("Clicking this button will allow you to"
            + " edit the selected company.");
    }//GEN-LAST:event_btnCompanyEditMouseEntered

    private void btnCompanyEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompanyEditActionPerformed
        setEditCompanyUI();
    }//GEN-LAST:event_btnCompanyEditActionPerformed

    private void btnCompanyAddMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCompanyAddMouseEntered
        txtLiveHelp.setText("Clicking the button will allow you to add"
            + " more companies.");
    }//GEN-LAST:event_btnCompanyAddMouseEntered

    private void btnCompanyAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompanyAddActionPerformed
        setAddCompanyUI();
    }//GEN-LAST:event_btnCompanyAddActionPerformed

    private void btnCompanyDoneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCompanyDoneMouseEntered
        txtLiveHelp.setText("Clicking the button will confirm the changes and add"
            + " a new company.");
    }//GEN-LAST:event_btnCompanyDoneMouseEntered

    private void btnCompanyDoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompanyDoneActionPerformed
        addCompany();
    }//GEN-LAST:event_btnCompanyDoneActionPerformed

    private void btnCompanyCancelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCompanyCancelMouseEntered
        txtLiveHelp.setText("Clicking this button will cancel the changes.");        
    }//GEN-LAST:event_btnCompanyCancelMouseEntered

    private void btnCompanyCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompanyCancelActionPerformed
        cancelAddEditCompany();
    }//GEN-LAST:event_btnCompanyCancelActionPerformed

    private void btnCompanySaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCompanySaveMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCompanySaveMouseClicked

    private void btnCompanySaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCompanySaveMouseEntered
        txtLiveHelp.setText("Clicking this button will confirm and make"
            + " changes to the system with the new values.");
    }//GEN-LAST:event_btnCompanySaveMouseEntered

    private void btnCompanySaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompanySaveActionPerformed
        editCompany();
    }//GEN-LAST:event_btnCompanySaveActionPerformed

    private void btnUserSearchMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUserSearchMouseEntered
        txtLiveHelp.setText("Click this button to search for a user.");
    }//GEN-LAST:event_btnUserSearchMouseEntered

    private void btnUserSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUserSearchActionPerformed
        searchUsers();
    }//GEN-LAST:event_btnUserSearchActionPerformed

    private void txtUserSearchMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtUserSearchMouseEntered
        txtLiveHelp.setText("Here you can enter a user to search for. "
            + "Once you have entered one click the search button.");
    }//GEN-LAST:event_txtUserSearchMouseEntered

    private void btnSearchUserClearMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSearchUserClearMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSearchUserClearMouseEntered

    private void btnSearchUserClearMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSearchUserClearMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSearchUserClearMouseExited

    private void btnSearchUserClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchUserClearActionPerformed
        txtUserSearch.setText("");
        this.userList.updateList();
        refreshList(lstUserList, userList);
        this.btnSearchUserClear.setVisible(false);
        this.txtUserSearch.setEditable(true);
    }//GEN-LAST:event_btnSearchUserClearActionPerformed

    private void lstUserListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstUserListMouseClicked
        IListable obj = getValue(this.userList, lstUserList);
        if(null != obj) {
            if(obj.getClass() == new User().getClass()) {
                User user = (User)obj;
                this.setUserTextFields(user);
            }
        }
    }//GEN-LAST:event_lstUserListMouseClicked

    private void lstUserListMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstUserListMouseEntered
        txtLiveHelp.setText("This is where all of the users are displayed"
            + ". Click on anyone of them to display more information about"
            + " the user.");
    }//GEN-LAST:event_lstUserListMouseEntered

    private void btnUserDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUserDeleteMouseEntered
        txtLiveHelp.setText("Clicking this button will allow you to"
            + " delete the selected user.");
    }//GEN-LAST:event_btnUserDeleteMouseEntered

    private void btnUserDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUserDeleteActionPerformed
        deleteUser();
    }//GEN-LAST:event_btnUserDeleteActionPerformed

    private void btnUserEditMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUserEditMouseEntered
        txtLiveHelp.setText("Clicking this button will allow you to"
            + " edit the selected user.");
    }//GEN-LAST:event_btnUserEditMouseEntered

    private void btnUserEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUserEditActionPerformed
       setEditUserUI();
    }//GEN-LAST:event_btnUserEditActionPerformed

    private void btnUserAddMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUserAddMouseEntered
        txtLiveHelp.setText("Clicking the button will allow you to add"
            + " more users.");
    }//GEN-LAST:event_btnUserAddMouseEntered

    private void btnUserAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUserAddActionPerformed
        setAddUserUI();
    }//GEN-LAST:event_btnUserAddActionPerformed

    private void btnUserDoneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUserDoneMouseEntered
        txtLiveHelp.setText("Clicking the button will confirm the changes and add"
            + "a new user.");        
    }//GEN-LAST:event_btnUserDoneMouseEntered

    private void btnUserDoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUserDoneActionPerformed
        addUser();
    }//GEN-LAST:event_btnUserDoneActionPerformed

    private void btnUserCancelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUserCancelMouseEntered
        txtLiveHelp.setText("Clicking this button will cancel the changes.");
    }//GEN-LAST:event_btnUserCancelMouseEntered

    private void btnUserCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUserCancelActionPerformed
        cancelAddEditUser();
    }//GEN-LAST:event_btnUserCancelActionPerformed

    private void btnUserSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUserSaveMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnUserSaveMouseClicked

    private void btnUserSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUserSaveMouseEntered
        txtLiveHelp.setText("Clicking this button will confirm and make"
            + " changes to the system with the new values.");
    }//GEN-LAST:event_btnUserSaveMouseEntered

    private void btnUserSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUserSaveActionPerformed
        editUser();
    }//GEN-LAST:event_btnUserSaveActionPerformed

    private void btnEventDoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEventDoneActionPerformed
       addEvent();
    }//GEN-LAST:event_btnEventDoneActionPerformed

    private void btnEventBookTicketMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventBookTicketMouseEntered
        txtLiveHelp.setText("This button will navigate you to the booking page with the selected events information.");
    }//GEN-LAST:event_btnEventBookTicketMouseEntered

    private void btnEventBookTicketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEventBookTicketActionPerformed
        IListable event = getValue(eventList, lstEventList);
        cmbTicketEvent.setSelectedItem(event.getName());
        tabPane.setSelectedIndex(3);
        setAddTicketUI();
    }//GEN-LAST:event_btnEventBookTicketActionPerformed

    private void btnUserBookTicketMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUserBookTicketMouseEntered
        txtLiveHelp.setText("This will take you to the tickets tab with this users details.");
    }//GEN-LAST:event_btnUserBookTicketMouseEntered

    private void btnUserBookTicketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUserBookTicketActionPerformed
       IListable user = getValue(userList, lstUserList);
       cmbTicketEvent.setSelectedItem(user.getName());
       tabPane.setSelectedIndex(3);
       setAddTicketUI();
    }//GEN-LAST:event_btnUserBookTicketActionPerformed

    private void btnEventEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEventEditActionPerformed
        setEditEventUI();
    }//GEN-LAST:event_btnEventEditActionPerformed

    private void cmbTicketTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTicketTypeActionPerformed
        txtTicketType.setText(cmbTicketType.getSelectedItem().toString());
    }//GEN-LAST:event_cmbTicketTypeActionPerformed

    private void btnEventAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEventAddActionPerformed
       setAddEventUI(); 
    }//GEN-LAST:event_btnEventAddActionPerformed

    private void btnEventEditCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEventEditCancelActionPerformed
        cancelAddEditEvent();
    }//GEN-LAST:event_btnEventEditCancelActionPerformed

    private void txtEventNameMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventNameMouseEntered
        txtLiveHelp.setText("This field is the name of the event.");
    }//GEN-LAST:event_txtEventNameMouseEntered

    private void txtEventVenueMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventVenueMouseEntered
        txtLiveHelp.setText("This is the venue where the event will be held.");
    }//GEN-LAST:event_txtEventVenueMouseEntered

    private void txtEventCompanyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventCompanyMouseEntered
        txtLiveHelp.setText("This is the company that will be hosting the event.");
    }//GEN-LAST:event_txtEventCompanyMouseEntered

    private void txtEventTypeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventTypeMouseEntered
        txtLiveHelp.setText("This is the type of event it will be.");
    }//GEN-LAST:event_txtEventTypeMouseEntered

    private void txtEventDescriptionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtEventDescriptionMouseEntered
        txtLiveHelp.setText("This is a short description of the event.");
    }//GEN-LAST:event_txtEventDescriptionMouseEntered

    private void txtEventSeatingPriceKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEventSeatingPriceKeyReleased
        if(!txtEventSeatingPrice.getText().contains("£")) {
        txtEventSeatingPrice.setText("£" + txtEventSeatingPrice.getText());
        }
    }//GEN-LAST:event_txtEventSeatingPriceKeyReleased

    private void txtEventStandingPriceKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEventStandingPriceKeyReleased
        if(!txtEventSeatingPrice.getText().contains("£")) {
        txtEventSeatingPrice.setText("£" + txtEventSeatingPrice.getText());
        }
    }//GEN-LAST:event_txtEventStandingPriceKeyReleased

    private void txtVenueNameMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtVenueNameMouseEntered
        txtLiveHelp.setText("This is the name of the venue.");
    }//GEN-LAST:event_txtVenueNameMouseEntered

    private void txtAddressOneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtAddressOneMouseEntered
        txtLiveHelp.setText("This is where the venue is located.");
    }//GEN-LAST:event_txtAddressOneMouseEntered

    private void txtPostcodeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtPostcodeMouseEntered
        txtLiveHelp.setText("This is the postcode of the venue.");
    }//GEN-LAST:event_txtPostcodeMouseEntered

    private void txtStandingSpacesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtStandingSpacesMouseEntered
        txtLiveHelp.setText("This is the amount of standing spaces that the venue will have.");
    }//GEN-LAST:event_txtStandingSpacesMouseEntered

    private void txtSeatedSpacesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtSeatedSpacesMouseEntered
        txtLiveHelp.setText("This is the amount of seated spaces that the venue will have.");
    }//GEN-LAST:event_txtSeatedSpacesMouseEntered

    private void txtMaxCapacityMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtMaxCapacityMouseEntered
        txtLiveHelp.setText("This is the max amount of space in the venue. note: this is automatically calculated.");
    }//GEN-LAST:event_txtMaxCapacityMouseEntered

    private void eventPopupMenuMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_eventPopupMenuMousePressed
        if (evt.isPopupTrigger()) {
            showPopupMenu(evt);
        }        
    }//GEN-LAST:event_eventPopupMenuMousePressed

    private void eventPopupMenuMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_eventPopupMenuMouseReleased
        if (evt.isPopupTrigger()) {
            showPopupMenu(evt);
        }
    }//GEN-LAST:event_eventPopupMenuMouseReleased

    private void btnGoToEventsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToEventsMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGoToEventsMouseClicked

    private void btnGoToEventsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToEventsMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGoToEventsMouseEntered

    private void btnGoToEventsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoToEventsActionPerformed
        tabPane.setSelectedIndex(1);
    }//GEN-LAST:event_btnGoToEventsActionPerformed

    private void btnGoToVenuesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToVenuesMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGoToVenuesMouseClicked

    private void btnGoToVenuesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToVenuesMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGoToVenuesMouseEntered

    private void btnGoToVenuesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoToVenuesActionPerformed
        tabPane.setSelectedIndex(2);
    }//GEN-LAST:event_btnGoToVenuesActionPerformed

    private void btnGoToTicketsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToTicketsMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGoToTicketsMouseClicked

    private void btnGoToTicketsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToTicketsMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGoToTicketsMouseEntered

    private void btnGoToTicketsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoToTicketsActionPerformed
        tabPane.setSelectedIndex(3);
    }//GEN-LAST:event_btnGoToTicketsActionPerformed

    private void btnGoToCompaniesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToCompaniesMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGoToCompaniesMouseClicked

    private void btnGoToCompaniesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToCompaniesMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGoToCompaniesMouseEntered

    private void btnGoToCompaniesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoToCompaniesActionPerformed
       tabPane.setSelectedIndex(4);
    }//GEN-LAST:event_btnGoToCompaniesActionPerformed

    private void btnGoToUsersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToUsersMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGoToUsersMouseClicked

    private void btnGoToUsersMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToUsersMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGoToUsersMouseEntered

    private void btnGoToUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoToUsersActionPerformed
        tabPane.setSelectedIndex(5);
    }//GEN-LAST:event_btnGoToUsersActionPerformed

    private void txtTicketUserMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtTicketUserMouseEntered
        txtLiveHelp.setText("This is the user that the ticket is linked to.");
    }//GEN-LAST:event_txtTicketUserMouseEntered

    private void txtTicketEventMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtTicketEventMouseEntered
        txtLiveHelp.setText("This is the event for the ticket.");
    }//GEN-LAST:event_txtTicketEventMouseEntered

    private void txtTicketTypeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtTicketTypeMouseEntered
        txtLiveHelp.setText("This is the type of ticket.");
    }//GEN-LAST:event_txtTicketTypeMouseEntered

    private void jPanelCompaniesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelCompaniesMouseEntered
        txtLiveHelp.setText("");
    }//GEN-LAST:event_jPanelCompaniesMouseEntered

    private void txtCompanyNameMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtCompanyNameMouseEntered
        txtLiveHelp.setText("This is the company name.");
    }//GEN-LAST:event_txtCompanyNameMouseEntered

    private void txtCompanyEmailAddressMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtCompanyEmailAddressMouseEntered
        txtLiveHelp.setText("This is the companies email address. Note: email address must contain a '@'.");
    }//GEN-LAST:event_txtCompanyEmailAddressMouseEntered

    private void txtCompanyPhoneNumberMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtCompanyPhoneNumberMouseEntered
        txtLiveHelp.setText("This is the companies phone number. note: the phone number must be 11 digits long.");
    }//GEN-LAST:event_txtCompanyPhoneNumberMouseEntered

    private void jPanelUsersMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelUsersMouseEntered
        txtLiveHelp.setText("");
    }//GEN-LAST:event_jPanelUsersMouseEntered

    private void txtUserFirstNameMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtUserFirstNameMouseEntered
        txtLiveHelp.setText("This is the users first name.");
    }//GEN-LAST:event_txtUserFirstNameMouseEntered

    private void txtUserLastNameMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtUserLastNameMouseEntered
        txtLiveHelp.setText("This is the users last name.");
    }//GEN-LAST:event_txtUserLastNameMouseEntered

    private void txtUserDOBMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtUserDOBMouseEntered
        txtLiveHelp.setText("This is the users date of birth. Note: use the format DD/MM/YY");
    }//GEN-LAST:event_txtUserDOBMouseEntered

    private void txtUserTelephoneNumberMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtUserTelephoneNumberMouseEntered
        txtLiveHelp.setText("This is the users telephone number. Note: number must be 11 digits.");
    }//GEN-LAST:event_txtUserTelephoneNumberMouseEntered

    private void txtUserEmailAddressMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtUserEmailAddressMouseEntered
        txtLiveHelp.setText("This is the users email. Note: email address must contain a '@'.");
    }//GEN-LAST:event_txtUserEmailAddressMouseEntered

    private void txtUserPasswordMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtUserPasswordMouseEntered
        txtLiveHelp.setText("This is the users password.");
    }//GEN-LAST:event_txtUserPasswordMouseEntered

    private void txtUserAddressMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtUserAddressMouseEntered
        txtLiveHelp.setText("This is the users address.");
    }//GEN-LAST:event_txtUserAddressMouseEntered

    private void txtUserPostCodeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtUserPostCodeMouseEntered
        txtLiveHelp.setText("This is the users postcode.");
    }//GEN-LAST:event_txtUserPostCodeMouseEntered

    private void btnEventHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEventHomeMouseEntered
        txtLiveHelp.setText("This button will take you back to the home screen.");
    }//GEN-LAST:event_btnEventHomeMouseEntered

    private void btnEventHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEventHomeActionPerformed
        tabPane.setSelectedIndex(0);
    }//GEN-LAST:event_btnEventHomeActionPerformed

    private void btnVenueHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVenueHomeMouseEntered
        txtLiveHelp.setText("This button will take you back to the home screen.");
    }//GEN-LAST:event_btnVenueHomeMouseEntered

    private void btnVenueHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVenueHomeActionPerformed
        tabPane.setSelectedIndex(0);
    }//GEN-LAST:event_btnVenueHomeActionPerformed

    private void btnTicketHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTicketHomeMouseEntered
        txtLiveHelp.setText("This button will take you back to the home screen.");
    }//GEN-LAST:event_btnTicketHomeMouseEntered

    private void btnTicketHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTicketHomeActionPerformed
        tabPane.setSelectedIndex(0);
    }//GEN-LAST:event_btnTicketHomeActionPerformed

    private void btnCompaniesHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCompaniesHomeMouseEntered
        txtLiveHelp.setText("This button will take you back to the home screen.");
    }//GEN-LAST:event_btnCompaniesHomeMouseEntered

    private void btnCompaniesHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompaniesHomeActionPerformed
        tabPane.setSelectedIndex(0);
    }//GEN-LAST:event_btnCompaniesHomeActionPerformed

    private void btnUsersHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUsersHomeMouseEntered
        txtLiveHelp.setText("This button will take you back to the home screen.");
    }//GEN-LAST:event_btnUsersHomeMouseEntered

    private void btnUsersHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUsersHomeActionPerformed
        tabPane.setSelectedIndex(0);
    }//GEN-LAST:event_btnUsersHomeActionPerformed

    private void cmbUserTypeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbUserTypeMouseEntered
        txtLiveHelp.setText("This is the user type. Note: selecting guest will autofill fields.");
    }//GEN-LAST:event_cmbUserTypeMouseEntered

    private void cmbUserTypePopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_cmbUserTypePopupMenuWillBecomeInvisible
        guestUserValidation();
    }//GEN-LAST:event_cmbUserTypePopupMenuWillBecomeInvisible

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        refreshAllLists();
    }//GEN-LAST:event_btnRefreshActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ListGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ListGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCompaniesHome;
    private javax.swing.JButton btnCompanyAdd;
    private javax.swing.JButton btnCompanyCancel;
    private javax.swing.JButton btnCompanyDelete;
    private javax.swing.JButton btnCompanyDone;
    private javax.swing.JButton btnCompanyEdit;
    private javax.swing.JButton btnCompanySave;
    private javax.swing.JButton btnCompanySearch;
    private javax.swing.JButton btnEventAdd;
    private javax.swing.JButton btnEventBookTicket;
    private javax.swing.JButton btnEventDelete;
    private javax.swing.JButton btnEventDone;
    private javax.swing.JButton btnEventEdit;
    private javax.swing.JButton btnEventEditCancel;
    private javax.swing.JButton btnEventHome;
    private javax.swing.JButton btnEventSave;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnGoToCompanies;
    private javax.swing.JButton btnGoToEvents;
    private javax.swing.JButton btnGoToTickets;
    private javax.swing.JButton btnGoToUsers;
    private javax.swing.JButton btnGoToVenues;
    private javax.swing.JButton btnLogHistoryClear;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnSearchCompanyClear;
    private javax.swing.JButton btnSearchEventClear;
    private javax.swing.JButton btnSearchUserClear;
    private javax.swing.JButton btnSearchVenueClear;
    private javax.swing.JButton btnTicketAdd;
    private javax.swing.JButton btnTicketCancel;
    private javax.swing.JButton btnTicketDelete;
    private javax.swing.JButton btnTicketDone;
    private javax.swing.JButton btnTicketEdit;
    private javax.swing.JButton btnTicketHome;
    private javax.swing.JButton btnTicketSave;
    private javax.swing.JButton btnUserAdd;
    private javax.swing.JButton btnUserBookTicket;
    private javax.swing.JButton btnUserCancel;
    private javax.swing.JButton btnUserDelete;
    private javax.swing.JButton btnUserDone;
    private javax.swing.JButton btnUserEdit;
    private javax.swing.JButton btnUserSave;
    private javax.swing.JButton btnUserSearch;
    private javax.swing.JButton btnUsersHome;
    private javax.swing.JButton btnVenueAdd;
    private javax.swing.JButton btnVenueCancel;
    private javax.swing.JButton btnVenueDelete;
    private javax.swing.JButton btnVenueDone;
    private javax.swing.JButton btnVenueEdit;
    private javax.swing.JButton btnVenueHome;
    private javax.swing.JButton btnVenueSave;
    private javax.swing.JButton btnVenueSearch;
    private javax.swing.JComboBox cmbCompany;
    private javax.swing.JComboBox cmbTicketEvent;
    private javax.swing.JComboBox cmbTicketType;
    private javax.swing.JComboBox cmbTicketUser;
    private javax.swing.JComboBox cmbUserType;
    private javax.swing.JComboBox cmbVenue;
    private javax.swing.JPanel editEventsPanel;
    private javax.swing.JLabel eventNameLbl;
    private javax.swing.JPopupMenu eventPopupMenu;
    private javax.swing.JLabel eventSystemLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanelCompanies;
    private javax.swing.JPanel jPanelEvents;
    private javax.swing.JPanel jPanelHome;
    private javax.swing.JPanel jPanelTickets;
    private javax.swing.JPanel jPanelUsers;
    private javax.swing.JPanel jPanelVenue;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JLabel lblAddressOne;
    private javax.swing.JLabel lblAgeRestriction;
    private javax.swing.JLabel lblCompany;
    private javax.swing.JLabel lblCompanyName;
    private javax.swing.JLabel lblEmailAddress;
    private javax.swing.JLabel lblEndDate;
    private javax.swing.JLabel lblEndTime;
    private javax.swing.JLabel lblEventDescription;
    private javax.swing.JLabel lblEventType;
    private javax.swing.JLabel lblLiveHelp;
    private javax.swing.JLabel lblLogHistory;
    private javax.swing.JLabel lblMaxCapacity;
    private javax.swing.JLabel lblPhoneNumber;
    private javax.swing.JLabel lblPostCode;
    private javax.swing.JLabel lblSeatedSpaces;
    private javax.swing.JLabel lblSeating;
    private javax.swing.JLabel lblSeatingPrice;
    private javax.swing.JLabel lblSeatingVenue;
    private javax.swing.JLabel lblStandingPrice;
    private javax.swing.JLabel lblStandingSpaces;
    private javax.swing.JLabel lblStartDate;
    private javax.swing.JLabel lblStartTime;
    private javax.swing.JLabel lblTicketEvent;
    private javax.swing.JLabel lblTicketType;
    private javax.swing.JLabel lblTicketUser;
    private javax.swing.JLabel lblUserAddress;
    private javax.swing.JLabel lblUserAddressTitle;
    private javax.swing.JLabel lblUserDOB;
    private javax.swing.JLabel lblUserEmailAddress;
    private javax.swing.JLabel lblUserFirstName;
    private javax.swing.JLabel lblUserLastName;
    private javax.swing.JLabel lblUserPassword;
    private javax.swing.JLabel lblUserPostCode;
    private javax.swing.JLabel lblUserTelephoneNumber;
    private javax.swing.JLabel lblUserType;
    private javax.swing.JLabel lblVenueName;
    private javax.swing.JList lstCompanyList;
    private javax.swing.JList lstEventList;
    private javax.swing.JList lstTicketList;
    private javax.swing.JList lstUserList;
    private javax.swing.JList lstVenueList;
    private javax.swing.JTabbedPane tabPane;
    private javax.swing.JTextField txtAddressOne;
    private javax.swing.JTextField txtCompanyEmailAddress;
    private javax.swing.JTextField txtCompanyName;
    private javax.swing.JTextField txtCompanyPhoneNumber;
    private javax.swing.JTextField txtCompanySearch;
    private javax.swing.JTextField txtEventAgeRestriction;
    private javax.swing.JTextField txtEventCompany;
    private javax.swing.JTextArea txtEventDescription;
    private javax.swing.JTextField txtEventEndDate;
    private javax.swing.JTextField txtEventEndTime;
    private javax.swing.JTextField txtEventName;
    private javax.swing.JTextField txtEventSearch;
    private javax.swing.JTextField txtEventSeatingPrice;
    private javax.swing.JTextField txtEventStandingPrice;
    private javax.swing.JTextField txtEventStartDate;
    private javax.swing.JTextField txtEventStartTime;
    private javax.swing.JTextField txtEventType;
    private javax.swing.JTextField txtEventVenue;
    private javax.swing.JTextArea txtLiveHelp;
    private javax.swing.JTextArea txtLogHistory;
    private javax.swing.JTextField txtMaxCapacity;
    private javax.swing.JTextField txtPostcode;
    private javax.swing.JTextField txtSeatedSpaces;
    private javax.swing.JTextField txtStandingSpaces;
    private javax.swing.JTextField txtTicketEvent;
    private javax.swing.JTextField txtTicketType;
    private javax.swing.JTextField txtTicketUser;
    private javax.swing.JTextField txtUserAddress;
    private javax.swing.JTextField txtUserDOB;
    private javax.swing.JTextField txtUserEmailAddress;
    private javax.swing.JTextField txtUserFirstName;
    private javax.swing.JTextField txtUserLastName;
    private javax.swing.JTextField txtUserPassword;
    private javax.swing.JTextField txtUserPostCode;
    private javax.swing.JTextField txtUserSearch;
    private javax.swing.JTextField txtUserTelephoneNumber;
    private javax.swing.JTextField txtUserType;
    private javax.swing.JTextField txtVenueName;
    private javax.swing.JTextField txtVenueSearch;
    private javax.swing.JLabel venueLbl;
    // End of variables declaration//GEN-END:variables

}
