/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lists;

import Behaviour.IListable;
import Behaviour.Ticket;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.util.ArrayList;

/**
 *
 * @author mvizard
 */
public class TicketList extends List {
    
    ArrayList<IListable> list;
    ArrayList<Ticket> ticketList;
    
    public TicketList() {
        list = new ArrayList<>();
        ticketList = new ArrayList<>();
        
        updateList();
    }
    
    public TicketList(String parameter, String value) {
        list = new ArrayList<>();
        ticketList = new ArrayList<>();
        
        updateListWhere(parameter, value);
    }
    
    @Override
    public Boolean addToList(IListable item) {
        Ticket ticket = (Ticket)item;
        
        try {
            ArrayList<String> ticketToAdd = new ArrayList<>();
            ticketToAdd.add("TICKETID");
            ticketToAdd.add("0");
            ticketToAdd.add("USERID");
            ticketToAdd.add(String.valueOf(ticket.getUserID()));
            ticketToAdd.add("EVENTID");
            ticketToAdd.add(String.valueOf(ticket.getEventID()));
            ticketToAdd.add("TICKETTYPE");
            ticketToAdd.add(ticket.getTicketType().toUpperCase());
            this.api.sendPost("Ticket", ticketToAdd);
            this.updateList();
            return true;
        } catch(Exception ex) {
            return false;
        }
    }

    @Override
    public Boolean editItem(IListable item) {
        Ticket newTicket = (Ticket)item;
             
        try {
            ArrayList<String> ticketToAdd = new ArrayList<>();
            ticketToAdd.add("TICKETID");
            ticketToAdd.add(String.valueOf(newTicket.getID()));
            ticketToAdd.add("USERID");
            ticketToAdd.add(String.valueOf(newTicket.getUserID()));
            ticketToAdd.add("EVENTID");
            ticketToAdd.add(String.valueOf(newTicket.getEventID()));
            ticketToAdd.add("TICKETTYPE");
            ticketToAdd.add(String.valueOf(newTicket.getTicketType()));

            this.api.sendPut("Ticket", ticketToAdd);
            this.updateList();
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
    
    @Override
    public ArrayList<IListable> getList() {
        ArrayList<IListable> returnList = this.list;
        return returnList;
    }
    
    public ArrayList<Ticket> getSpecificList() {
        ArrayList<Ticket> returnList = this.ticketList;
        return returnList;
    }

    @Override
    public Boolean updateList() {
        String stringList = super.api.sendGet("Ticket");
 
        try {  
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }

        return null != stringList;
    }
    
     public Boolean updateListWhere(String parameter, String idValue) {
        String stringList = this.api.sendGet("Ticket", parameter, idValue);
        try {  
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }

        return null != stringList;
    }
    
    public void parseJson(String stringList) {
        ticketList.clear();
        list.clear();
        JsonElement j = new JsonParser().parse(stringList);   
        try {
            JsonArray jArray = j.getAsJsonArray(); 
            for(JsonElement element : jArray) {
                setTicketFromGson(element);
            }
        } catch(Exception ex) {
            setTicketFromGson(j);
        }
    }
    
    public void setTicketFromGson(JsonElement j) {
        Gson gson = new Gson();
        Ticket obj = gson.fromJson(j, Ticket.class);
        obj.setEvent();
        obj.setUser();
        ticketList.add(obj);
        list.add(obj);
    }

    @Override
    public Boolean clear() {
        this.list.clear();
        this.ticketList.clear();
        return true;
    }

    @Override
    public Boolean deleteItem(IListable item) {
        Ticket ticket = (Ticket) item;
        api.sendDelete("Ticket", String.valueOf(ticket.getID()));
        updateList();
        return true;
    }
}
