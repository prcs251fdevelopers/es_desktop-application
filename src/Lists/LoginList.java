/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lists;

import Behaviour.IListable;
import Behaviour.Login;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.security.MessageDigest;
import java.util.ArrayList;

/**
 *
 * @author Max
 */
public class LoginList extends List {

    ArrayList<Login> loginList;

    public LoginList() {
        loginList = new ArrayList<>();
        list = new ArrayList<>();
        
        updateList();
    }
    
    public LoginList(String parameter, String value) {
        loginList = new ArrayList<>();
        list = new ArrayList<>();
        
        updateListWhere(parameter, value);
    }
     
    @Override
    public Boolean clear() {
        return super.clear(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean deleteItem(IListable item) {
        Login login = (Login) item;
        Boolean result = api.sendDelete("venue", String.valueOf(login.getID()));
        updateList();
        return result;
    }

    @Override
    public Boolean editItem(IListable item) {
        Login login = (Login)item;
        String password = login.getName();
        password = hashPassword(password);
        try {
            ArrayList<String> loginToAdd = new ArrayList<>();
            loginToAdd.add("LOGINID");
            loginToAdd.add(String.valueOf(login.getID()));
            loginToAdd.add("PASSWORD");
            loginToAdd.add(password);

            return null != this.api.sendPut("login", loginToAdd);
        } catch(Exception ex) {
            return null;
        }
    }
    
    public String editItemString(IListable item) {
        Login login = (Login)item;
        String password = login.getName();
        password = hashPassword(password);
        try {
            ArrayList<String> loginToAdd = new ArrayList<>();
            loginToAdd.add("LOGINID");
            loginToAdd.add(String.valueOf(login.getID()));
            loginToAdd.add("PASSWORD");
            loginToAdd.add(password);

            return this.api.sendPut("login", loginToAdd);
        } catch(Exception ex) {
            return null;
        }
    }
    
    public String addToListString(IListable item) {
        Login login = (Login)item;
        String password = login.getName();
        password = hashPassword(password);
        try {
            ArrayList<String> loginToAdd = new ArrayList<>();
            loginToAdd.add("LOGINID");
            loginToAdd.add("0");
            loginToAdd.add("PASSWORD");
            loginToAdd.add(password);

            return this.api.sendPost("login", loginToAdd);
        } catch(Exception ex) {
            return null;
        }
    }

    @Override
    public Boolean addToList(IListable item) {
        return super.addToList(item); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean updateList() {
        String stringList = super.api.sendGet("login");
        
        try {
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }


        return null != stringList;
    }
    
    public Boolean updateListWhere(String parameter, String idValue) {
        String stringList = this.api.sendGet("login", parameter, idValue);
        
        try {
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }

        return null != stringList;
    }
    
    public void parseJson(String stringList) {
        loginList.clear();
        list.clear();
            JsonElement j = new JsonParser().parse(stringList);  
            try {
                JsonArray jArray = j.getAsJsonArray(); 
                for(JsonElement element : jArray) {
                    setLoginFromGson(element);
                }
            } catch(Exception ex) {
                setLoginFromGson(j);
            }

    }
    
    public void setLoginFromGson(JsonElement j) {
        Gson gson = new Gson();
        Login obj = gson.fromJson(j, Login.class);
        loginList.add(obj);
        list.add(obj);
    }
    
    public String hashPassword(String password) { 
        //Login login = (Login)item;
        
        //String password = login.getName();
        
        try {
            MessageDigest digest = MessageDigest.getInstance( "SHA-1" );
            byte[] bytes = password.getBytes("UTF-8");
            digest.update(bytes, 0, bytes.length);
            bytes = digest.digest();

            char[] hexArray = "0123456789ABCDEF".toCharArray();
            char[] hexChars = new char[ bytes.length * 2 ];
            for( int j = 0; j < bytes.length; j++ )
            {
                int v = bytes[ j ] & 0xFF;
                hexChars[ j * 2 ] = hexArray[ v >>> 4 ];
                hexChars[ j * 2 + 1 ] = hexArray[ v & 0x0F ];
            }
            password = new String(hexChars);
        }
        catch (Exception e) {
        }
        return password;
    }

    @Override
    public ArrayList<IListable> getList() {
        return super.getList(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
