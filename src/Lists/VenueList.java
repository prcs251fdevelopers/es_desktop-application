/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lists;

import Behaviour.IListable;
import Behaviour.Venue;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.util.ArrayList;

/**
 *
 * @author Max
 */
public class VenueList extends List {
    
    ArrayList<IListable> list;
    ArrayList<Venue> venueList;
    
    
    public VenueList() {
        list =  new ArrayList<>();
        venueList = new ArrayList<>();
        
        updateList();
    }
    
    public VenueList(String parameter, String idValue) {
        list =  new ArrayList<>();
        venueList = new ArrayList<>();
        
        updateListWhere(parameter, idValue);
    }
    
    public ArrayList<Venue> getVenueList() {
        ArrayList<Venue> result = this.venueList;
        return result;
    }

//    this.VENUENAME = venueName;
//        this.ADDRESSLINE1 = addressLine1;
//        this.ADDRESSLINE2 = addressLine2;
//        this.POSTCODE = postCode;
//        this.MAXIMUMCAPCITY = maxCapacity;
//        this.SEATEDSPACES = seatedSpace;
//        this.STANDINGSPACES = standingSpace;
    
    @Override
    public Boolean addToList(IListable item) {
         Venue venue = (Venue)item;
        
        try {
            ArrayList<String> venueToAdd = new ArrayList<>();
            venueToAdd.add("VENUEID");
            venueToAdd.add("0");
            venueToAdd.add("VENUENAME");
            venueToAdd.add(String.valueOf(venue.getVenueName()).toUpperCase());
            venueToAdd.add("ADDRESSLINE1");
            venueToAdd.add(String.valueOf(venue.getADDRESSLINE1()).toUpperCase());
            venueToAdd.add("POSTCODE");
            venueToAdd.add(String.valueOf(venue.getPOSTCODE()).toUpperCase());
            venueToAdd.add("MAXIMUMCAPACITY");
            venueToAdd.add(String.valueOf(venue.getMAXIMUMCAPACITY()).toUpperCase());
            venueToAdd.add("SEATINGSPACES");
            venueToAdd.add(String.valueOf(venue.getSEATEDSPACES()).toUpperCase());
            venueToAdd.add("STANDINGSPACES");
            venueToAdd.add(String.valueOf(venue.getSTANDINGSPACES()).toUpperCase());
            this.api.sendPost("venue", venueToAdd);
            this.updateList();
            return true;
        } catch(Exception ex) {
            return false;
        }
    }

    @Override
    public Boolean editItem(IListable item) {
        Venue venue = (Venue)item;
        Boolean result = false;
        try {
            ArrayList<String> venueToAdd = new ArrayList<>();
            venueToAdd.add("VENUEID");
            venueToAdd.add(String.valueOf(venue.getID()));
            venueToAdd.add("VENUENAME");
            venueToAdd.add(String.valueOf(venue.getVenueName()).toUpperCase());
            venueToAdd.add("ADDRESSLINE1");
            venueToAdd.add(String.valueOf(venue.getADDRESSLINE1()).toUpperCase());
            venueToAdd.add("POSTCODE");
            venueToAdd.add(String.valueOf(venue.getPOSTCODE()).toUpperCase());
            venueToAdd.add("MAXIMUMCAPACITY");
            venueToAdd.add(String.valueOf(venue.getMAXIMUMCAPACITY()).toUpperCase());
            venueToAdd.add("SEATINGSPACES");
            venueToAdd.add(String.valueOf(venue.getSEATEDSPACES()).toUpperCase());
            venueToAdd.add("STANDINGSPACES");
            venueToAdd.add(String.valueOf(venue.getSTANDINGSPACES()).toUpperCase());
            this.api.sendPut("venue", venueToAdd);

            this.updateList();
            result = true;
            return result;
        } catch(Exception ex) {
            return result;
        }
    }
    
    @Override
    public Boolean deleteItem(IListable item) {
        Venue venue = (Venue) item;
        Boolean result = api.sendDelete("venue", String.valueOf(venue.getID()));
        updateList();
        return result;
    }

    @Override
    public ArrayList<IListable> getList() {
        ArrayList<IListable> returnList = this.list;
        return returnList;
    }

    @Override
    public Boolean updateList() {
        String stringList = super.api.sendGet("venue");
        try {  
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }

        return null != stringList;
    }
    
     public Boolean updateListWhere(String parameter, String idValue) {
        String stringList = this.api.sendGet("venue", parameter, idValue);
        try {  
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }
        return null != stringList;
    }
    
    public void parseJson(String stringList) {
        venueList.clear();
        list.clear();
        JsonElement j = new JsonParser().parse(stringList);    
        try {
            JsonArray jArray = j.getAsJsonArray(); 
            for(JsonElement element : jArray) {
                setVenueFromGson(element);
            }
        } catch(Exception ex) {
            setVenueFromGson(j);
        }
    }
    
    public void setVenueFromGson(JsonElement j) {
        Gson gson = new Gson();
        Venue obj = gson.fromJson(j, Venue.class);
        venueList.add(obj);
        list.add(obj);
    }

    @Override
    public Boolean clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
