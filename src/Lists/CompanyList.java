/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lists;

import Behaviour.Company;
import Behaviour.IListable;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.util.ArrayList;

/**
 *
 * @author Max
 */
public class CompanyList extends List {
    
    ArrayList<Company> companyList;
    
    
    public CompanyList() {
        companyList = new ArrayList<>();
        list = new ArrayList<>();
        
        updateList();
    }
    
    public CompanyList(String parameter, String value) {
        companyList = new ArrayList<>();
        list = new ArrayList<>();
        
        updateListWhere(parameter, value);
    }

    @Override
    public Boolean addToList(IListable item) {
        Company company = (Company)item; 
        try {
            ArrayList<String> companyToAdd = new ArrayList<>();
            companyToAdd.add("COMPANYID");
            companyToAdd.add("0");
            companyToAdd.add("COMPANYNAME");
            companyToAdd.add(String.valueOf(company.getName()).toUpperCase());
            companyToAdd.add("PHONENUMBER");
            companyToAdd.add(String.valueOf(company.getPHONENUMBER()).toUpperCase());
            companyToAdd.add("EMAILADDRESS");
            companyToAdd.add(String.valueOf(company.getEMAILADDRESS()).toUpperCase());
            if(null != this.api.sendPost("Company", companyToAdd)) {
                this.updateList();
                return true;
            }
            return false;
        } catch(Exception ex) {
            return false;
        }
    }

    @Override
    public Boolean deleteItem(IListable item) {
        Company company = (Company) item;
        Boolean result = api.sendDelete("company", String.valueOf(company.getID()));
        updateList();
        return result;
    }

    @Override
    public Boolean editItem(IListable item) {
        Company company = (Company)item; 
        Boolean result = false;
        try {
            ArrayList<String> companyToAdd = new ArrayList<>();
            companyToAdd.add("COMPANYID");
            companyToAdd.add(String.valueOf(company.getID()));
            companyToAdd.add("COMPANYNAME");
            companyToAdd.add(String.valueOf(company.getName()).toUpperCase());
            companyToAdd.add("PHONENUMBER");
            companyToAdd.add(String.valueOf(company.getPHONENUMBER()).toUpperCase());
            companyToAdd.add("EMAILADDRESS");
            companyToAdd.add(String.valueOf(company.getEMAILADDRESS()).toUpperCase());
            this.api.sendPut("Company", companyToAdd);
            this.updateList();
            result = true;
            return result;
        } catch(Exception ex) {
            return result;
        }
    }

    @Override
    public ArrayList<IListable> getList() {
        ArrayList<IListable> returnList = this.list;
        return returnList;
    }
    
    public ArrayList<Company> getCompanyList() {
        ArrayList<Company> returnList = this.companyList;
        return returnList;
    }

    @Override
    public Boolean updateList() {
        String stringList = super.api.sendGet("Company");
        
        try {  
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }

        return null != stringList;
    }
    
    public Boolean updateListWhere(String parameter, String idValue) {
        
        String stringList = this.api.sendGet("Company", parameter, idValue);
        try {  
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }

        return null != stringList;
    }
    
    public void parseJson(String stringList) {
        companyList.clear();
        list.clear();
        JsonElement j = new JsonParser().parse(stringList);     
        try {
            JsonArray jArray = j.getAsJsonArray(); 
            for(JsonElement element : jArray) {
                setCompanyFromGson(element);
            }
        } catch(Exception ex) {
            setCompanyFromGson(j);
        }
    }
    
    public void setCompanyFromGson(JsonElement j) {
        Gson gson = new Gson();
        Company obj = gson.fromJson(j, Company.class);
        companyList.add(obj);
        list.add(obj);
    }
    
    @Override
    public Boolean clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }   
}
