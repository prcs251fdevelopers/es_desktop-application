/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lists;

import Behaviour.Address;
import Behaviour.IListable;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.util.ArrayList;

/**
 *
 * @author Max
 */
public class AddressList extends List {

    protected ArrayList<Address> addressList;
    
    public AddressList() {
        addressList = new ArrayList<>();
        list = new ArrayList<>();
        
        updateList();
    }
    
    public AddressList(String parameter, String value) {
        addressList = new ArrayList<>();
        list = new ArrayList<>();
        
        updateListWhere(parameter, value);
    }
    
    @Override
    public Boolean clear() {
        return super.clear(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean deleteItem(IListable item) {
        Address address = (Address) item;
        Boolean result = api.sendDelete("address", String.valueOf(address.getID()));
        updateList();
        return result;
    }

    @Override
    public Boolean editItem(IListable item) {
         Address address = (Address)item;
        
        try {
            ArrayList<String> addressToAdd = new ArrayList<>();
            addressToAdd.add("ADDRESSID");
            addressToAdd.add(String.valueOf(address.getID()));
            addressToAdd.add("ADDRESSLINE");
            addressToAdd.add(address.getName().toUpperCase());
            addressToAdd.add("POSTCODE");
            addressToAdd.add(address.getPOSTCODE().toUpperCase());

            return null != this.api.sendPut("address", addressToAdd);
        } catch(Exception ex) {
            return null;
        }
    }
    
    public String editItemString(IListable item) {
         Address address = (Address)item;
        
        try {
            ArrayList<String> addressToAdd = new ArrayList<>();
            addressToAdd.add("ADDRESS");
            addressToAdd.add(String.valueOf(address.getID()));
            addressToAdd.add("ADDRESSLINE");
            addressToAdd.add(address.getName());
            addressToAdd.add("POSTCODE");
            addressToAdd.add(address.getPOSTCODE());

            return this.api.sendPut("address", addressToAdd);
        } catch(Exception ex) {
            return null;
        }
    }

    public String addToListString(IListable item) {
        Address address = (Address)item;
        
        try {
            ArrayList<String> addressToAdd = new ArrayList<>();
            addressToAdd.add("ADDRESSID");
            addressToAdd.add("0");
            addressToAdd.add("ADDRESSLINE");
            addressToAdd.add(address.getName());
            addressToAdd.add("POSTCODE");
            addressToAdd.add(address.getPOSTCODE());

            return this.api.sendPost("address", addressToAdd);
        } catch(Exception ex) {
            return null;
        }
    }
    
    @Override
    public Boolean addToList(IListable item) {
        return super.addToList(item); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean updateList() {
        addressList.clear();
        list.clear();
        String stringList = super.api.sendGet("address");
        try {  
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }

        return null != stringList;
    }
    
    @Override
    public Boolean updateListWhere(String parameter, String idValue) { 
        String stringList = this.api.sendGet("address", parameter, idValue);
        try {
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }
        return null != stringList;
    }
    
    @Override
    public void parseJson(String stringList) {   
        addressList.clear();
        list.clear();
        JsonElement j = new JsonParser().parse(stringList);    
        try { 
            JsonArray jArray = j.getAsJsonArray(); 
            for(JsonElement element : jArray) {
                setAddressFromGson(element);
            }
        } catch(Exception ex) {
            setAddressFromGson(j);
        }
        
    }
    
    public void setAddressFromGson(JsonElement j) {
        Gson gson = new Gson();
        Address obj = gson.fromJson(j, Address.class);
        addressList.add(obj);
        list.add(obj);
    }

    @Override
    public ArrayList<IListable> getList() {
        return super.getList();
    }
    
}
