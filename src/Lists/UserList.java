/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lists;

import Behaviour.IListable;
import Behaviour.User;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.util.ArrayList;

/**
 *
 * @author mvizard
 */
public class UserList extends List {
    
    ArrayList<User> userList;
    
    public UserList() {
        userList = new ArrayList<>();
        list = new ArrayList<>();
        
        updateList();
    }
    
    public UserList(String parameter, String value) {
        userList = new ArrayList<>();
        list = new ArrayList<>();
        
        updateListWhere(parameter, value);
    }
    
    @Override
    public Boolean clear() {
        return super.clear(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean deleteItem(IListable item) {
        User user = (User) item;
        Boolean result = api.sendDelete("systemuser", String.valueOf(user.getID()));
        updateList();
        return result;
    }

    @Override
    public Boolean editItem(IListable item) {
        User user = (User)item;
        
        try {
            ArrayList<String> userToAdd = new ArrayList<>();
            userToAdd.add("USERID");
            userToAdd.add(String.valueOf(user.getID()));
            userToAdd.add("LOGINID");
            userToAdd.add(String.valueOf(user.getLOGINID()));
            userToAdd.add("ADDRESSID");
            userToAdd.add(String.valueOf(user.getADDRESSID()));
            userToAdd.add("FIRSTNAME");
            userToAdd.add(user.getFIRSTNAME().toUpperCase());
            userToAdd.add("LASTNAME");
            userToAdd.add(user.getLASTNAME().toUpperCase());
            userToAdd.add("EMAILADDRESS");
            userToAdd.add(user.getEMAILADDRESS().toUpperCase());
            userToAdd.add("DOB");
            userToAdd.add(user.getDOB());
            userToAdd.add("TELEPHONENUMBER");
            userToAdd.add(user.getTELEPHONENUMBER());
            userToAdd.add("USERTYPE");
            userToAdd.add(user.getUSERTYPE().toUpperCase());

            this.api.sendPut("SystemUser", userToAdd);
            this.updateList();
            return true;
        } catch(Exception ex) {
            return false;
        }
    }

    @Override
    public Boolean addToList(IListable item) {
        User user = (User)item;
        
        try {
            ArrayList<String> userToAdd = new ArrayList<>();
            userToAdd.add("USERID");
            userToAdd.add("0");
            userToAdd.add("LOGINID");
            userToAdd.add(String.valueOf(user.getLOGINID()));
            userToAdd.add("ADDRESSID");
            userToAdd.add(String.valueOf(user.getADDRESSID()));
            userToAdd.add("FIRSTNAME");
            userToAdd.add(user.getFIRSTNAME().toUpperCase());
            userToAdd.add("LASTNAME");
            userToAdd.add(user.getLASTNAME().toUpperCase());
            userToAdd.add("EMAILADDRESS");
            userToAdd.add(user.getEMAILADDRESS().toUpperCase());
            userToAdd.add("DOB");
            userToAdd.add(user.getDOB());
            userToAdd.add("TELEPHONENUMBER");
            userToAdd.add(user.getTELEPHONENUMBER());
            userToAdd.add("USERTYPE");
            userToAdd.add(user.getUSERTYPE().toUpperCase());

            this.api.sendPost("systemuser", userToAdd);
            this.updateList();
            return true;
        } catch(Exception ex) {
            return false;
        }
    }

    @Override
    public Boolean updateList() {
        String stringList = super.api.sendGet("Systemuser");
        try {  
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }

        return null != stringList;
    }
    
    public Boolean updateListWhere(String parameter, String idValue) {     
        String stringList = this.api.sendGet("Systemuser", parameter, idValue);
        try {  
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }

        return null != stringList;
    }
    
    public void parseJson(String stringList) {
        userList.clear();
        list.clear();
        JsonElement j = new JsonParser().parse(stringList);    
        try {
            JsonArray jArray = j.getAsJsonArray(); 
            for(JsonElement element : jArray) {
                setUserFromGson(element);
            }
        } catch(Exception ex) {
            setUserFromGson(j);
        }
    }
    
    public void setUserFromGson(JsonElement j) {
        Gson gson = new Gson();
        User obj = gson.fromJson(j, User.class);
        obj.setLogin();
        obj.setAddress();
        userList.add(obj);
        list.add(obj);
    }


    @Override
    public ArrayList<IListable> getList() {
        ArrayList<IListable> returnList = this.list;
        return returnList;
    }
    
    public ArrayList<User> getUserList() {
        ArrayList<User> returnList = this.userList;
        return returnList;
    }
    
//    public ArrayList<User> getListWhere(String idValue) {
//        ArrayList<User> result = new ArrayList<>();
//        String stringList = this.api.sendGet("systemuser", idValue);
//        try {  
//            JsonElement j = new JsonParser().parse(stringList);
//            JsonArray jArray = j.getAsJsonArray();      
//            Gson gson = new Gson();
//
//            for(JsonElement element : jArray) {
//                User obj = gson.fromJson(element, User.class);
//                //obj.setVenue();
//                //obj.setCompany();
//                result.add(obj);
//            }
//        } catch(Exception ex) {
//            //return false;
//        }
//        
//        return result;
//    }
    
}
