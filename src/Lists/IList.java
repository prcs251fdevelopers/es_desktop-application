/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lists;

import Behaviour.IListable;
import java.util.ArrayList;

/**
 *
 * @author Max
 */
public interface IList{
    //public void refreshList();
    //public Boolean setObject(String itemOne, String itemTwo, String itemThree, String itemFour, 
    //        String itemFive, String itemSix, String itemSeven, String itemEight, String itemNine);
    //public IListable getObject();
    public Boolean addToList(IListable item);
    public Boolean editItem(IListable item);
    public Boolean deleteItem(IListable item);
    public ArrayList<IListable> getList();
    public Boolean updateList();
    public Boolean updateListWhere(String parameter, String idValue);
    public void parseJson(String stringList);
    public Boolean clear();
}
