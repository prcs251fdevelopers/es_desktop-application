/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lists;

import Behaviour.Event;
import Behaviour.IListable;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.util.ArrayList;

/**
 *
 * @author Max
 */
public class EventList extends List {
    
    ArrayList<Event> eventList;
    CompanyList companyList;
    
    public EventList() {
        eventList = new ArrayList<>();
        list = new ArrayList<>();
        companyList = new CompanyList();
        
        updateList();
    }
    
    public EventList(String parameter, String value) {
        eventList = new ArrayList<>();
        list = new ArrayList<>();
        companyList = new CompanyList();
        
        updateListWhere(parameter, value);
    }

//    @Override
//    public Boolean addToList(IListable item) {
//        Event event = (Event)item;
//        Gson gson = new Gson();
//        try {
//            //JsonObject jsonObject = new JsonObject();
//            JsonElement jsonElement = gson.toJsonTree(event);
//            JsonObject jsonObject = jsonElement.getAsJsonObject();
//            jsonObject.remove("EVENTID");
//            jsonObject.addProperty("EVENTID", "null");
//            String jsonString = jsonObject.toString();
//            //String jsonString = gson.toJson(event);
//            //jsonString = "EVENT=" + jsonString;
//            this.api.sendPost("Events", jsonString);
//            this.updateList();
//            return true;
//        } catch(Exception ex) {
//            return false;
//        }
//    }
    @Override
    public Boolean addToList(IListable item) {
        Event event = (Event)item;
        Boolean result = false;
        try {
            ArrayList<String> eventToAdd = new ArrayList<>();
            eventToAdd.add("EVENTID");
            eventToAdd.add("0");
            eventToAdd.add("COMPANYID");
            eventToAdd.add(String.valueOf(event.getCompanyID()).toUpperCase());
            eventToAdd.add("VENUEID");
            eventToAdd.add(String.valueOf(event.getVenueID()).toUpperCase());
            eventToAdd.add("EVENTNAME");
            eventToAdd.add(event.getEventName().toUpperCase());
            eventToAdd.add("EVENTTYPE");
            eventToAdd.add(event.getEventType().toUpperCase());
            eventToAdd.add("STARTTIME");
            eventToAdd.add(event.getStartTime().toUpperCase());
            eventToAdd.add("ENDTIME");
            eventToAdd.add(event.getEndTime().toUpperCase());
            eventToAdd.add("STARTDATE");
            eventToAdd.add(event.getSTARTDATE().toUpperCase());
            eventToAdd.add("ENDDATE");
            eventToAdd.add(event.getENDDATE().toUpperCase());
            eventToAdd.add("SEATINGPRICE");
            eventToAdd.add(event.getSeatingPrice().toUpperCase());
            eventToAdd.add("STANDINGPRICE");
            eventToAdd.add(event.getStandingPrice().toUpperCase());
            eventToAdd.add("AGERESTRICTION");
            eventToAdd.add(event.getAgeRestriction().toUpperCase());
            eventToAdd.add("EVENTDESCRIPTION");
            eventToAdd.add(event.getEVENTDESCRIPTION());

            if(null != this.api.sendPost("Event", eventToAdd)) {      
                this.updateList();
                result = true;
            }
            return result;
        } catch(Exception ex) {
            return result;
        }
    }

    @Override
    public Boolean editItem(IListable item) {
        Event event = (Event)item;
        Boolean result = false;
        try {
            ArrayList<String> eventToAdd = new ArrayList<>();
            eventToAdd.add("EVENTID");
            eventToAdd.add(String.valueOf(event.getID()).toUpperCase());
            eventToAdd.add("COMPANYID");
            eventToAdd.add(String.valueOf(event.getCompanyID()).toUpperCase());
            eventToAdd.add("VENUEID");
            eventToAdd.add(String.valueOf(event.getVenueID()).toUpperCase());
            eventToAdd.add("EVENTNAME");
            eventToAdd.add(event.getEventName().toUpperCase());
            eventToAdd.add("EVENTTYPE");
            eventToAdd.add(event.getEventType().toUpperCase());
            eventToAdd.add("STARTTIME");
            eventToAdd.add(event.getStartTime().toUpperCase());
            eventToAdd.add("ENDTIME");
            eventToAdd.add(event.getEndTime().toUpperCase());
            eventToAdd.add("SEATINGPRICE");
            eventToAdd.add(event.getSeatingPrice().toUpperCase());
            eventToAdd.add("STANDINGPRICE");
            eventToAdd.add(event.getStandingPrice().toUpperCase());
            eventToAdd.add("AGERESTRICTION");
            eventToAdd.add(event.getAgeRestriction());
            eventToAdd.add("EVENTDESCRIPTION");
            eventToAdd.add(event.getEVENTDESCRIPTION().toUpperCase());
            eventToAdd.add("STARTDATE");
            eventToAdd.add(event.getSTARTDATE().toUpperCase());
            eventToAdd.add("ENDDATE");
            eventToAdd.add(event.getENDDATE().toUpperCase());

            this.api.sendPut("Event", eventToAdd);
            this.updateList();
            result = true;

            return result;
        } catch(Exception ex) {
            return result;
        }
    }
    
    @Override
    public Boolean deleteItem(IListable item) {
        Event event = (Event)item;
        Boolean result = api.sendDelete("event", String.valueOf(event.getID()));
        updateList();
        return result;
    }

    @Override
    public ArrayList<IListable> getList() {
        return super.getList();
    }
    
    @Override
    public Boolean updateList() {
        String stringList = super.api.sendGet("event");
        try {  
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }

        return null != stringList;
    }
    
    public Boolean updateListWhere(String parameter, String idValue) {

        String stringList = this.api.sendGet("event", parameter, idValue);
        try {  
            parseJson(stringList);
        } catch(Exception ex) {
            return false;
        }

        return null != stringList;
    }
    
    public void parseJson(String stringList) {
        eventList.clear();
        list.clear();
        JsonElement j = new JsonParser().parse(stringList);  
            try {
                JsonArray jArray = j.getAsJsonArray(); 
                for(JsonElement element : jArray) {
                    setEventFromGson(element);
                }
            } catch(Exception ex) {
                setEventFromGson(j);
            }
    }
    
    public void setEventFromGson(JsonElement j) {
        Gson gson = new Gson();
        Event obj = gson.fromJson(j, Event.class);
        obj.setVenue();
        obj.setCompany();
        eventList.add(obj);
        list.add(obj);
    }
    

    @Override
    public Boolean clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    

    public ArrayList<Event> getEventList() {
        ArrayList<Event> resultList = this.eventList;
        return resultList;
    }
    
}
