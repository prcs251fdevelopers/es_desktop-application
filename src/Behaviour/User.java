/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behaviour;

import Lists.AddressList;
import Lists.LoginList;

/**
 *
 * @author Max
 */
public class User implements IListable {
    protected float USERID;
    protected float LOGINID;
    protected float ADDRESSID;
    protected String FIRSTNAME;
    protected String LASTNAME;
    protected String EMAILADDRESS;
    protected String DOB;
    protected String TELEPHONENUMBER;
    protected String USERTYPE;
    
    protected Address userAddress;
    protected Login userLogin;
    
    public User() {
        
    }

    public User(float LOGINID, float ADDRESSID, String FIRSTNAME, String LASTNAME, String EMAILADDRESS, String DOB, String TELEPHONENUMBER, String USERTYPE) {
        this.LOGINID = LOGINID;
        this.ADDRESSID = ADDRESSID;
        this.FIRSTNAME = FIRSTNAME;
        this.LASTNAME = LASTNAME;
        this.EMAILADDRESS = EMAILADDRESS;
        this.DOB = DOB;
        this.TELEPHONENUMBER = TELEPHONENUMBER;
        this.USERTYPE = USERTYPE;
        
        getUserAddress();
        getUserLogin();
    }
    
    
    @Override
    public int getID() {
        return (int)USERID;
    }

    @Override
    public String getName() {
        return this.FIRSTNAME + " " + this.LASTNAME;
    }

    public String getFIRSTNAME() {
        return FIRSTNAME;
    }

    public String getLASTNAME() {
        return LASTNAME;
    }

    public String getEMAILADDRESS() {
        return EMAILADDRESS;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getTELEPHONENUMBER() {
        return TELEPHONENUMBER;
    }

    public void setTELEPHONENUMBER(String TELEPHONENUMBER) {
        this.TELEPHONENUMBER = TELEPHONENUMBER;
    }

    public int getLOGINID() {
        return (int)LOGINID;
    }

    public void setLOGINID(float LOGINID) {
        this.LOGINID = LOGINID;
    }
    
    public int getADDRESSID() {
        return (int)ADDRESSID;
    }

    public void setADDRESSID(float ADDRESSID) {
        this.ADDRESSID = ADDRESSID;
    }

    public String getUSERTYPE() {
        return USERTYPE;
    }

    public void setUSERTYPE(String USERTYPE) {
        this.USERTYPE = USERTYPE;
    }

    public Address getUserAddress() {
        if(null != userAddress) {
            return userAddress;
        } else {
            Address tempAddress = new Address(1, "GUEST", "GUEST");
            return tempAddress;
        }
    }

    public Login getUserLogin() {
        if(null != userLogin) {
            return userLogin;
        } else {
            Login tempLogin = new Login(1, "GUEST");
            return tempLogin;
        }
    }
    
    public void setLogin() {
        LoginList tempLoginList = new LoginList("id", String.valueOf(LOGINID));
        
        try {
            for(IListable currLogin : tempLoginList.getList()) {
                if(currLogin.getID() == this.getLOGINID()) {
                    this.userLogin = (Login) currLogin;
                    break;
                }
            }
        } catch(Exception ex) {
            
        }
    }
    
    public void setAddress() {
        AddressList tempVenueList = new AddressList("id", String.valueOf(ADDRESSID));
        
        try {
            for(IListable currAddress : tempVenueList.getList()) {
                if(currAddress.getID() == this.getADDRESSID()) {
                    this.userAddress = (Address) currAddress;
                    break;
                }
            }
        } catch(Exception ex) {
            
        }
    }

    public void setFIRSTNAME(String FIRSTNAME) {
        this.FIRSTNAME = FIRSTNAME;
    }

    public void setLASTNAME(String LASTNAME) {
        this.LASTNAME = LASTNAME;
    }

    public void setEMAILADDRESS(String EMAILADDRESS) {
        this.EMAILADDRESS = EMAILADDRESS;
    }
    
    
}
