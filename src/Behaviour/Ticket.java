/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behaviour;

import Lists.EventList;
import Lists.UserList;

/**
 *
 * @author mvizard
 */
public class Ticket implements IListable  {
    
    protected int TICKETID;
    protected int USERID;
    protected int EVENTID;
    protected String TICKETTYPE;
    
    protected Event event;
    protected User user;
    
    public Ticket() {  
        //setEvent();
        //setUser();
    }
    
    public Ticket(int userID, int eventID, String ticketType) {
        //this.TICKETID = ticketID;
        this.USERID = userID;
        this.EVENTID = eventID;
        this.TICKETTYPE = ticketType;
        setEvent();
        setUser();
    }
    
    public void setUser() {
        UserList tempList = new UserList("id", String.valueOf(USERID));
        
        //tempList.updateListWhere();
        
        for(IListable currUser : tempList.getList()) {
            if(this.USERID == currUser.getID()) {
                this.user = (User)currUser;
                break;
            }
        }
    }
    
    public void setEvent() {
        //should call a select where statement but searches the whole list for now
        EventList tempList = new EventList("id", String.valueOf(EVENTID));
        
        for(IListable currEvent : tempList.getList()) {
            if(this.EVENTID == currEvent.getID()) {
                this.event = (Event)currEvent;
                break;
            }
        }
    }

    @Override
    public int getID() {
        return this.TICKETID;
    }

    public void setTicketID(int ticketID) {
        this.TICKETID = ticketID;
    }

    public int getUserID() {
        return USERID;
    }

    public void setUserID(int userID) {
        this.USERID = userID;
        setUser();
    }

    public int getEventID() {
        return EVENTID;
    }

    public void setEventID(int eventID) {
        this.EVENTID = eventID;
        setEvent();
    }

    public String getTicketType() {
        return TICKETTYPE;
    }

    public void setTicketType(String ticketType) {
        this.TICKETTYPE = ticketType;
    }

    @Override
    public String getName() {
        return user.getFIRSTNAME() + " " + user.getLASTNAME() + " - " + event.getName();
        //return "ticketName";
    }

    public Event getEvent() {
        return event;
    }

    public User getUser() {
        return user;
    }  
}
