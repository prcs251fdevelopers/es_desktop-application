/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behaviour;

import Lists.CompanyList;
import Lists.VenueList;

/**
 *
 * @author Max
 */
public class Event implements IListable {
    
    protected int EVENTID;
    protected int COMPANYID;
    protected int VENUEID;
    protected String EVENTNAME;
    protected String EVENTTYPE;
    protected String STARTTIME;
    protected String ENDTIME;
    protected String STARTDATE;
    protected String ENDDATE;
    protected String SEATINGPRICE;
    protected String STANDINGPRICE;
    protected String AGERESTRICTION;
    protected String EVENTDESCRIPTION;
    
    transient protected APIConnection api;
    
    protected Venue venue;
    protected Company company;
    
    
    //protected Boolean ageRestriction;
    public Event() {
        //this.api = new APIConnection();
        
    }
    
    public Event(int companyID, int venueID, String eventName, String eventType, 
            String startTime, String endTime, String startDate, String endDate, String standingPrice, String seatingPrice, String ageRestriction, String eventDescription) {
        this.COMPANYID = companyID;
        this.VENUEID = venueID;
        this.EVENTNAME = eventName;
        this.EVENTTYPE = eventType;
        this.STARTTIME = startTime;
        this.ENDTIME = endTime;
        this.STARTDATE = startDate;
        this.ENDDATE = endDate;
        this.SEATINGPRICE = seatingPrice;
        this.STANDINGPRICE = standingPrice;
        this.AGERESTRICTION = ageRestriction;
        this.EVENTDESCRIPTION = eventDescription;
        
        this.api = new APIConnection();

    }
    public int getID() {
        return this.EVENTID;
    }

    public long getCompanyID() {
        return this.COMPANYID;
    }
    
    public String getEventName() {
        return EVENTNAME;
    }

    public void setEventName(String eventName) {
        this.EVENTNAME = eventName;
    }
    
    public int getVenueID() {
        return this.VENUEID;
    }

    public Venue getVenue() {
        return venue;
    }
    
    public Boolean setAgeRestriction(String ageRestriction) {
            this.AGERESTRICTION = ageRestriction;
            return true;

    }
    
    public void setVenueID(int newID) {
        this.VENUEID = newID;
        setVenue();
    }
    
    public void setVenue() {
        VenueList tempVenueList = new VenueList("id", String.valueOf(VENUEID));
        
        try {
            for(IListable currVenue : tempVenueList.getList()) {
                if(currVenue.getID() == this.getVenueID()) {
                    this.venue = (Venue) currVenue;
                    break;
                }
            }
        } catch(Exception ex) {
            
        }
    }

    public String getCompanyName() {
        return company.getName();
    }
    
    public void setCompanyID(int newID) {
        this.COMPANYID = newID;
        setCompany();
    }
    
    public void setCompany() {
        CompanyList tempCompanyList = new CompanyList("id", String.valueOf(COMPANYID));
        
        try {
            for(IListable currCompany : tempCompanyList.getList()) {
                if(currCompany.getID() == this.getCompanyID()) {
                    this.company =  (Company) currCompany;
                    break;
                }
            }
        } catch(Exception ex) {
            
        }
        //this.company = (Company) companyList.getList().get(toIntExact(this.COMPANYID));
    }

    public String getEventType() {
        return EVENTTYPE;
    }

    public void setEventType(String eventType) {
        this.EVENTTYPE = eventType;
    }

    public String getStartTime() {
        return STARTTIME;
    }

    public void setStartTime(String startTime) {
        this.STARTTIME = startTime;
    }

    public String getEndTime() {
        return ENDTIME;
    }

    public void setEndTime(String endTime) {
        this.ENDTIME = endTime;
    }

    public String getStandingPrice() {
        return STANDINGPRICE;
    }

    public void setStandingPrice(String standingPrice) {
        this.STANDINGPRICE = standingPrice;
    }

    public String getSeatingPrice() {
        return SEATINGPRICE;
    }

    public void setSeatingPrice(String seatingPrice) {
        this.SEATINGPRICE = seatingPrice;
    }

    public String getAgeRestriction() {
        return this.AGERESTRICTION;//.substring(0, AGERESTRICTION.length() - 2);
    }

    @Override
    public String getName() {
        return this.EVENTNAME;
    }

    public Company getCompany() {
        return this.company;
    }

    public String getSTARTDATE() {
        return STARTDATE;
    }

    public void setSTARTDATE(String STARTDATE) {
        this.STARTDATE = STARTDATE;
    }

    public String getENDDATE() {
        return ENDDATE;
    }

    public void setENDDATE(String ENDDATE) {
        this.ENDDATE = ENDDATE;
    }

    public String getEVENTDESCRIPTION() {
        return EVENTDESCRIPTION;
    }

    public void setEVENTDESCRIPTION(String EVENTDESCRIPTION) {
        this.EVENTDESCRIPTION = EVENTDESCRIPTION;
    }
    
    
    
}
