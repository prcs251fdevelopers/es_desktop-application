/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behaviour;

/**
 *
 * @author Max
 */
public class Venue implements IListable  {
    
    protected int VENUEID;
    protected String VENUENAME;
    protected String ADDRESSLINE1;
    protected String POSTCODE;
    protected float MAXIMUMCAPACITY;
    protected float SEATINGSPACES;
    protected float STANDINGSPACES;
    
    public Venue() {
        //this.apiConnection
    }
    
    public Venue(String venueName, String addressLine1, String postCode, int maxCapacity, int seatedSpace, int standingSpace) {
        this.VENUENAME = venueName;
        this.ADDRESSLINE1 = addressLine1;
        this.POSTCODE = postCode;
        this.MAXIMUMCAPACITY = maxCapacity;
        this.SEATINGSPACES = seatedSpace;
        this.STANDINGSPACES = standingSpace;
    }
    
    @Override
    public int getID() {
        return this.VENUEID;
    }
    
    @Override
    public String getName() {
        return this.VENUENAME;
    }
    
    public String getVenueName() {
        return this.VENUENAME;
    }
    
    public Boolean setVenueName(String newName) {
        this.VENUENAME = newName;
        return true;
    }
    
    public Boolean setAddressOne(String address) {
        this.ADDRESSLINE1 = address;
        return true;
    }
    
    public Boolean setPostCode(String postCode) {
        this.POSTCODE = postCode;
        return true;
    }
    
    public Boolean setMaximumCapacity() {
        this.MAXIMUMCAPACITY = this.SEATINGSPACES + this.STANDINGSPACES;
        return true;
    }
    
    public Boolean setMaximumCapacityTest(int cap) {
        this.MAXIMUMCAPACITY = cap;
        return true;
    }
    
    public Boolean setSeatedSpace(int seats) {
        this.SEATINGSPACES = seats;
        return true;
    }
    
    public Boolean setStandingSpace(int standingSpace) {
        this.STANDINGSPACES = standingSpace;
        return true;
    }

    public String getADDRESSLINE1() {
        return ADDRESSLINE1;
    }
    
    public String getPOSTCODE() {
        return POSTCODE;
    }

    public int getMAXIMUMCAPACITY() {
        return (int)MAXIMUMCAPACITY;
    }

    public int getSEATEDSPACES() {
        return (int)SEATINGSPACES;
    }

    public int getSTANDINGSPACES() {
        return (int)STANDINGSPACES;
    }
    
    
}
