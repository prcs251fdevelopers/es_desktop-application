/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behaviour;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


/**
 *
 * @author Max
 */
public class APIConnection {
    
    public String sendGet(String table) {
        try {
            String url =  "http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/" + table;
            //String url =  "http://localhost:19090/" + table;
            
            URL obj = new URL(url);
            
            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
            
            conn.setRequestMethod("GET");
            
           //conn.setRequestProperty("User-Agent", USER_AGENT);
            
            BufferedReader r = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
            
            String input;
            String fullInput = "";
            
            while((input = r.readLine()) != null) {
                fullInput += input;
            }
            
            conn.disconnect();
            return fullInput;
        } catch (Exception ex) {
            return null;
        }
    }
    
    public String sendGet(String table, String parameter, String idValue) {
        try {
            String url =  "http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/" + table + "?" + parameter + "=" + idValue;
            //String url =  "http://localhost:19090/" + table;

            URL obj = new URL(url);
            
            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
            
            conn.setRequestMethod("GET");
   
            BufferedReader r = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
            
            String input;
            String fullInput = "";
            
            while((input = r.readLine()) != null) {
                fullInput += input;
            }
            conn.disconnect();
            return fullInput;         
        } catch (Exception ex) {
            return null;
        }
    }
    
    public String sendPut(String table, ArrayList<String> pAndV) {
        URL url;
        HttpURLConnection conn;

        try {
            url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/" + table);
            
            String urlParameters = "";//  = "param1=a&param2=b&param3=c";
            
            for(int i = 0; i < pAndV.size(); i = i + 2) {
                urlParameters += pAndV.get(i) + "=" + URLEncoder.encode(pAndV.get(i + 1), "UTF-8") + "&";
            }
            
            urlParameters = urlParameters.substring(0, urlParameters.length() - 1);
            
            //byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
            
            conn = (HttpURLConnection)url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("PUT");

            conn.setFixedLengthStreamingMode(urlParameters.getBytes().length);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(urlParameters);
            out.close();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String next = bufferedReader.readLine();
            conn.disconnect();
            return next;
        }
        catch(Exception e) {
            return null;
        }
    }
    
    
    public String sendPost(String table, ArrayList<String> pAndV) { 
        URL url;
        HttpURLConnection conn;

        try {
            url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/" + table);
            
            String urlParameters = "";//  = "param1=a&param2=b&param3=c";
            
            for(int i = 0; i < pAndV.size(); i = i + 2) {
                urlParameters += pAndV.get(i) + "=" + URLEncoder.encode(pAndV.get(i + 1), "UTF-8") + "&";
            }
            
            urlParameters = urlParameters.substring(0, urlParameters.length() - 1);
            
            //byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
            
            conn = (HttpURLConnection)url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");

            conn.setFixedLengthStreamingMode(urlParameters.getBytes().length);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(urlParameters);
            out.close();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String next = bufferedReader.readLine();
            conn.disconnect();
            return next;
        }
        catch(Exception e) {
            return null;
        }
    }
    
   
    
//    public Boolean sendPost(String table, ArrayList<String> pAndV) {
//        try {
//            String url =  "http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/" + table;
//            //String url =  "http://localhost:19090/" + table;
//            
//            String urlParameters = "";//  = "param1=a&param2=b&param3=c";
//            
//            for(int i = 0; i < pAndV.size(); i = i + 2) {
//                urlParameters += pAndV.get(i) + "=" + pAndV.get(i + 1) + "&";
//            }
//            
//            urlParameters = urlParameters.substring(0, urlParameters.length() - 1);
//            
//            byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
//            int    postDataLength = postData.length;
//            
//
//            URL obj = new URL(url);
//            
//            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
//            
//            conn.setRequestMethod("POST");
//            
//            conn.setDoOutput(true);
//            conn.setInstanceFollowRedirects( false );
//            //conn.setRequestMethod("POST");
//            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
//            conn.setRequestProperty("charset", "utf-8");
//            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
//            conn.setUseCaches(false);
    
//            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
//                throw new RuntimeException("Failed : HTTP error code : "
//                    + conn.getResponseCode());
//            }
//            
//            try( DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
//               wr.write(postData);
//            }
//            
//            BufferedReader r = new BufferedReader(
//                new InputStreamReader(conn.getInputStream()));       
//            
//            return true;
//        } catch (Exception ex) {
//            return false;
//        }
//    }
    
    public Boolean sendDelete(String table, String idValue) {
        try {
            String url =  "http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/" + table + "/" + idValue;
            //String url =  "http://localhost:19090/" + table;
            //String url =  "http://localhost:1686/" + table + "/5";       

            
            URL obj = new URL(url);
            
            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
            
            conn.setRequestMethod("DELETE");
            
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
            conn.setRequestProperty("charset", "utf-8");
            conn.setUseCaches(false);
            
            BufferedReader r = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
            
            conn.disconnect();
            
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    
}
