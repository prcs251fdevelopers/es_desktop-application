/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behaviour;

/**
 *
 * @author Max
 */
public class Address implements IListable {

    protected float ADDRESSID;
    protected String ADDRESSLINE;
    protected String POSTCODE;

    public Address() {
    }
    
    
    public Address(String addressLine, String postcode) {
        this.ADDRESSLINE = addressLine;
        this.POSTCODE = postcode;
    }
    
    public Address(int ADDRESSID, String addressLine, String postcode) {
        this.ADDRESSID = ADDRESSID;
        this.ADDRESSLINE = addressLine;
        this.POSTCODE = postcode;
    }
    
    @Override
    public int getID() {
        return (int)this.ADDRESSID;
    }

    @Override
    public String getName() {
        return this.ADDRESSLINE;
    }

    public void setADDRESSLINE(String ADDRESSLINE) {
        this.ADDRESSLINE = ADDRESSLINE;
    }
    
    
    public String getPOSTCODE() {
        return POSTCODE;
    }

    public void setPOSTCODE(String POSTCODE) {
        this.POSTCODE = POSTCODE;
    }    
}
