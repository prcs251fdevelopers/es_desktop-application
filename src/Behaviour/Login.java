/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behaviour;

/**
 *
 * @author Max
 */
public class Login implements IListable {
    
    protected float LOGINID;
    protected String PASSWORD;
    
    protected APIConnection conn;

    public Login() {
    }
    
    public Login(String password) {
        this.PASSWORD = password;
        conn = new APIConnection();
    }
    
    public Login(int LOGINID, String password) {
        this.LOGINID = LOGINID;
        this.PASSWORD = password;
        conn = new APIConnection();
    }
    
    @Override
    public int getID() {
        return (int)this.LOGINID;
    }

    @Override
    public String getName() {
        return this.PASSWORD;
    }

    public APIConnection getConn() {
        return conn;
    }

    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }
    
    
}
