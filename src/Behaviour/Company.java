/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Behaviour;

/**
 *
 * @author Max
 */
public class Company implements IListable {
    protected int COMPANYID;
    protected String COMPANYNAME;
    protected String PHONENUMBER;
    protected String EMAILADDRESS;

    public Company() {
    }

    public Company(String COMPANYNAME, String PHONENUMBER, String EMAILADDRESS) {
        this.COMPANYNAME = COMPANYNAME;
        this.PHONENUMBER = PHONENUMBER;
        this.EMAILADDRESS = EMAILADDRESS;
    }


    @Override
    public int getID() {
        return this.COMPANYID;
    }

    @Override
    public String getName() {
        return this.COMPANYNAME;
    }

    public void setCOMPANYNAME(String COMPANYNAME) {
        this.COMPANYNAME = COMPANYNAME;
    }

    public String getPHONENUMBER() {
        return PHONENUMBER;
    }

    public void setPHONENUMBER(String PHONENUMBER) {
        this.PHONENUMBER = PHONENUMBER;
    }

    public String getEMAILADDRESS() {
        return EMAILADDRESS;
    }

    public void setEMAILADDRESS(String EMAILADDRESS) {
        this.EMAILADDRESS = EMAILADDRESS;
    }
    
    
}
